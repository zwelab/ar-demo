#import "UIVerticalSlider.h"

@implementation UIVerticalSlider

- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super initWithCoder:decoder];
	if (self) {
		[self initVerticalSlider];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initVerticalSlider];
	}
	return self;
}

- (void)initVerticalSlider
{
    _slider = [[UISlider alloc] initWithFrame:self.frame];
    
    [self addSubview:_slider];
    
    CGRect rect = CGRectMake(0, 0, self.frame.size.height, self.frame.size.width );
    _slider.frame = rect;
    _slider.center = CGPointMake(self.frame.size.width/2,self.frame.size.height /2);
	_slider.transform = CGAffineTransformMakeRotation(-M_PI_2);
	
 
	
	[_slider setMinimumValueImage:_slider.minimumValueImage];
	[_slider setMaximumValueImage:_slider.maximumValueImage];
    [_slider setMinimumTrackTintColor:[UIColor grayColor]];
   
	//[_slider setThumbImage:[UIImage imageNamed:@"opacity"] forState:UIControlStateNormal];
    //[_slider setThumbImage:[UIImage imageNamed:@"opacity"] forState:UIControlStateHighlighted];
    [_slider setThumbImage:[_slider thumbImageForState:UIControlStateNormal] forState:UIControlStateNormal];
    [_slider setThumbImage:[_slider thumbImageForState:UIControlStateHighlighted] forState:UIControlStateHighlighted];
//	[_slider setThumbImage:[_slider thumbImageForState:UIControlStateHighlighted] forState:UIControlStateHighlighted];
	[_slider setThumbImage:[_slider thumbImageForState:UIControlStateSelected] forState:UIControlStateSelected];
	[_slider setThumbImage:[_slider thumbImageForState:UIControlStateDisabled] forState:UIControlStateDisabled];

}



- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    CGRect rect = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height );
    _slider.frame = rect;
    _slider.center = CGPointMake(self.bounds.size.width/2,self.bounds.size.height /2);
    _slider.transform = CGAffineTransformMakeRotation(-M_PI_2);
}



- (void)setMinimumValueImage:(UIImage *)image
{
	[_slider setMinimumValueImage:[self rotatedImage:image]];
}

- (void)setMaximumValueImage:(UIImage *)image
{
	[_slider setMaximumValueImage:[self rotatedImage:image]];
}

- (void)setThumbImage:(UIImage *)image forState:(UIControlState)state
{
	[_slider setThumbImage:[self rotatedImage:image] forState:state];
}

- (UIImage *)rotatedImage:(UIImage *)image
{
	if (!image) {
		return nil;
	}
	UIGraphicsBeginImageContextWithOptions(CGSizeMake(image.size.height, image.size.width), NO, image.scale);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextRotateCTM(context, M_PI_2);
	[image drawAtPoint:CGPointMake(0.0, -image.size.height)];
	return UIGraphicsGetImageFromCurrentImageContext();
}

@end
