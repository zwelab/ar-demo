
#import "UIBatteryView.h"

@implementation UIBatteryView{
    
    __weak IBOutlet UILabel *batteryLabel;
    __weak IBOutlet UIImageView *batteryInnerImage;
    __weak IBOutlet UIImageView *batteryImage;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    
    self = [super initWithCoder:decoder];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [[UINib nibWithNibName:@"UIBatteryView" bundle:nil] instantiateWithOwner:self options:nil];
        [self.view setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.view];
        self.view.frame = self.bounds;
        [self initView];
    }
    return self;
}

float x;

-(void) initView
{
    x = 0;
    [batteryLabel setText:@""];
    [self setImage:@"UIBatteryEmpty"];
   
}


-(void)setBatterylevel:(float)_value
{
    
    [batteryInnerImage.layer removeAllAnimations];
    batteryInnerImage.alpha = 1.0;
    [batteryLabel setTextColor:[UIColor colorWithRed:137.0/255.0 green:137.0/255.0 blue:137.0/255.0 alpha:1]];
    if (_value == -1)
    {
        [batteryLabel setText:@"None"];
        [batteryInnerImage setImage:nil];
    }
    
    if (_value >= 0.0 && _value < 0.05)
    {
        [batteryLabel setText:@"Low"];
        //[batteryLabel setTextColor:[UIColor colorWithRed:255.0/255.0 green:0 blue:0 alpha:1]];
        [self setInnerImage:@"UIBatteryInnerEmpty"];
        
        [UIView animateWithDuration:0.8 delay:0.0 options:
         UIViewAnimationOptionCurveEaseInOut |
         UIViewAnimationOptionRepeat |
         UIViewAnimationOptionAutoreverse |
         UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self->batteryInnerImage.alpha = 0.0f;
                         }
                         completion:nil];
    }

    if (_value >= 0.05 && _value < 0.25)
    {
        [batteryLabel setText:@"Low"];
        //[batteryLabel setTextColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0 alpha:1]];
        [self setInnerImage:@"UIBatteryInnerEmpty"];
    }

    if (_value >= 0.25 && _value < 0.50)
    {
        [batteryLabel setText:@"Mid"];
        //[batteryLabel setTextColor:[UIColor colorWithRed:243.0/255.0 green:155.0/255.0 blue:24.0/255.0 alpha:1]];
        [self setInnerImage:@"UIBatteryInnerHalf"];
    }

    if (_value >= 0.50 && _value <= 1)
    {
        [batteryLabel setText:@"Full"];
        //[batteryLabel setTextColor:[UIColor colorWithRed:0/255.0 green:255/255.0 blue:0.0/255.0 alpha:1]];
        [self setInnerImage:@"UIBatteryInnerFull"];
    }
}

-(void) setImage:(NSString*)_imageName
{
    UIImage * landscapeImage = [UIImage imageNamed:_imageName];
    UIImage * portraitImage = [[UIImage alloc] initWithCGImage: landscapeImage.CGImage
                                                         scale: 1.0
                                                   orientation: UIImageOrientationLeft];
    [batteryImage setImage:portraitImage ];
}

-(void) setInnerImage:(NSString*)_imageName
{
    UIImage * landscapeImage = [UIImage imageNamed:_imageName];
    UIImage * portraitImage = [[UIImage alloc] initWithCGImage: landscapeImage.CGImage
                                                         scale: 1.0
                                                   orientation: UIImageOrientationLeft];
    [batteryInnerImage setImage:portraitImage ];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
