

#import "UILButton.h"

@implementation UILButton
@synthesize  lzText = _lzText;

-(NSString*) lzText
{
    return _lzText;
}

-(void) setLzText:(NSString*)lzText

{
    [self setTitle:[[NSBundle mainBundle] localizedStringForKey:lzText value:@"" table:nil] forState:UIControlStateNormal];

     // info
}

-(void) setLzTextAttr:(NSString*)lzText

{
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithAttributedString:[self attributedTitleForState:UIControlStateNormal]];
    [string replaceCharactersInRange:NSMakeRange(0, string.length) withString:[[NSBundle mainBundle] localizedStringForKey:lzText value:@"" table:nil]];
    
    [self setAttributedTitle:string forState:UIControlStateNormal];
    
}
@end
