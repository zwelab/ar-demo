
#import "UIScanTableViewCell.h"

@implementation UIScanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.m_backgroundView.layer.cornerRadius = 5;
    self.m_backgroundView.layer.masksToBounds =  YES;

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
