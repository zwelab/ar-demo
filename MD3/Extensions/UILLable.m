
#import <Foundation/Foundation.h>
#import "UILLable.h"


@implementation UILLabel

@synthesize  lzText = _lzText;
    
-(NSString*) lzText
{
        return _lzText;
}
    
-(void) setLzText:(NSString*)lzText
{
    [self setText:[[NSBundle mainBundle] localizedStringForKey:lzText value:@"" table:nil]];
    _lzText = lzText;
 
}
@end
