

#import "UILSegementControl.h"

@implementation UILSegementControl

@synthesize  lzText1 = _lzText1;
@synthesize  lzText2 = _lzText2;
@synthesize  lzText3 = _lzText3;

-(NSString*) lzText1
{
    return _lzText1;
}

-(NSString*) lzText2
{
    return _lzText2;
}

-(NSString*) lzText3
{
    return _lzText3;
}

-(void) setLzText1:(NSString*)lzText
{
    [self setTitle:[[NSBundle mainBundle] localizedStringForKey:lzText value:@"" table:nil] forSegmentAtIndex:0];
}

-(void) setLzText2:(NSString*)lzText
{
    [self setTitle:[[NSBundle mainBundle] localizedStringForKey:lzText value:@"" table:nil] forSegmentAtIndex:1];
}

-(void) setLzText3:(NSString*)lzText
{
    [self setTitle:[[NSBundle mainBundle] localizedStringForKey:lzText value:@"" table:nil] forSegmentAtIndex:2];
}
@end
