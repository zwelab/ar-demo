

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RadarViewDelegate <NSObject>
-(void) setGyroOutOfRange:(int) _state;
@end

@interface UIRadarView : UIView{
    
    IBOutlet UIView *view;
    __weak IBOutlet UIImageView *m_point_Gyro;
    __weak IBOutlet UIImageView *m_point_Accel;
    __weak IBOutlet UIImageView *m_circleBack;
}

-(void) startMotionManager;
-(void) stopMotionManager;
-(void) resetPosition;
    
@property (nonatomic, weak) id<RadarViewDelegate> delegate;
    
@end

NS_ASSUME_NONNULL_END
