
#import <CoreMotion/CoreMotion.h>
#import "UIRadarView.h"

@implementation UIRadarView{
    
    CMMotionManager* motionManager;
    NSTimer *timer;
    
    float leftGyroBorder;
    float rightGyroBorder;
    double accelRadius;
    CGPoint center_accel;
    CGPoint center_gyro;
    
    BOOL gyroOutOfRange;
    BOOL acelOutOfRange;
    
    NSMutableData* x_gyro;
    NSMutableData* y_gyro;
    int     x_gyro_index;
    int     y_gyro_index;
    double calib_x;
    double calib_y;
    double z_move_start;
    CGPoint g_center0;
    BOOL g_gyro_need_reload;
    BOOL g_accel_need_reload;
    BOOL m_managerWork;
}

    
- (id)initWithCoder:(NSCoder *)decoder
{
    
    self = [super initWithCoder:decoder];
    if (self) {
        m_managerWork = NO;
        [[UINib nibWithNibName:@"UIRadarView" bundle:nil] instantiateWithOwner:self options:nil];
        [self addSubview:self->view];
        self->view.frame = self.bounds;
        [self setBackgroundColor:[UIColor clearColor]];
        [self initView];
        [self resetPosition];
        [self startMotionManager];
    }
    return self;
}
    
-(void) initView{
    x_gyro = [NSMutableData dataWithLength:sizeof(float) * 10];
    y_gyro = [NSMutableData dataWithLength:sizeof(float) * 10];
    leftGyroBorder = -0.18;
    rightGyroBorder = 0.18;
    accelRadius = (m_circleBack.frame.size.width * 0.5) * 0.10;
    motionManager = [CMMotionManager new];
}
    
-(void) resetPosition{
    g_accel_need_reload = YES;
    g_gyro_need_reload = YES;
}
    
-(void) startMotionManager{
    if (m_managerWork)
        return;
    m_managerWork = YES;
    motionManager.accelerometerUpdateInterval = 0.03;
    motionManager.gyroUpdateInterval = 0.03;
    
    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:
     ^(CMAccelerometerData * _Nullable accelerometerData, NSError * _Nullable error) {
         [self accelerateUpdate: accelerometerData.acceleration.x: accelerometerData.acceleration.y: accelerometerData.acceleration.z];
     }];
    
    [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:
     ^(CMDeviceMotion* motion, NSError* error) {
         [self gyroUpdate: motion.attitude.roll: motion.attitude.pitch: motion.attitude.yaw];
     }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(refreshGyroStateData)
                                           userInfo:nil
                                            repeats:YES];
}
    
-(void) stopMotionManager{
    [motionManager stopAccelerometerUpdates];
    [motionManager stopDeviceMotionUpdates];
    [timer invalidate];
    m_managerWork = NO;
}
    
-(void)accelerateUpdate: (float)x : (float)y : (float)z
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized(self)
        {
            [self accelerateUpdateMainThread:x:y:z];
        }
    });
}
    
-(void)accelerateUpdateMainThread: (float)x : (float)y : (float)z
{
    if (g_accel_need_reload)
    {
        
        g_accel_need_reload = NO;
        calib_x = x;
        calib_y = y;
        center_accel = m_circleBack.center;
        return;
    }
    
    float *x_gyro_loc = (float *)[x_gyro mutableBytes];
    float *y_gyro_loc = (float *)[y_gyro mutableBytes];
    
    if (x_gyro_index == 10)
    {
        for (int i = 1 ; i < x_gyro_index; i++) {
            x_gyro_loc[i-1] = x_gyro_loc[i];
        }
        x_gyro_loc[x_gyro_index-1] = x;
    }
    else
    {
        x_gyro_loc[x_gyro_index++] = x;
    }
    
    
    if (y_gyro_index == 10)
    {
        for (int i = 1 ; i < y_gyro_index; i++) {
            y_gyro_loc[i-1] = y_gyro_loc[i];
        }
        y_gyro_loc[y_gyro_index-1] = y;
    }
    else
    {
        y_gyro_loc[y_gyro_index++] = y;
    }
    
    float new_x =0;
    for (int i = 0 ; i < x_gyro_index; i++) {
        new_x += x_gyro_loc[i];
    }
    new_x /= (float)x_gyro_index;
    
    float new_y =0;
    for (int i = 0 ; i < y_gyro_index; i++) {
        new_y += y_gyro_loc[i];
    }
    new_y /= (float)y_gyro_index;
    
    x = new_x;
    y = new_y;
    
    
    
    
    const double x1 = (x - calib_x)*60;
    const double y1 = (y - calib_y)*-60;
    const double R = accelRadius;
    
    
    m_point_Accel.hidden = NO;
    if (x1*x1 + y1*y1 < R*R)
    {
        center_accel.x = m_circleBack.center.x + x1;
        center_accel.y = m_circleBack.center.y + y1;
        [m_point_Accel setCenter:center_accel];
        m_point_Accel.image = [UIImage imageNamed:@"RadarAssetYellowPoint"];
        acelOutOfRange = NO;
    }
    else
    {
        CGPoint vector;
        vector.x = x1;
        vector.y = y1;
        float vectorlen = sqrt(vector.x * vector.x + vector.y * vector.y);
        vector.x /= vectorlen;
        vector.y /= vectorlen;
        
        center_accel.x = m_circleBack.center.x + vector.x * R;
        center_accel.y = m_circleBack.center.y + vector.y * R;
        
        [m_point_Accel setCenter:center_accel];
        m_point_Accel.image = [UIImage imageNamed:@"RadarAssetRedPoint"];
        acelOutOfRange = YES;
    }
}
    
-(void)gyroUpdate: (float)x : (float)y : (float)z
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            @synchronized(self)
            {
                [self gyroUpdateMainThread:x:y:z];
            }
        });
    }
    
#define ACCEL_ANGLE  3.14159
-(void)gyroUpdateMainThread: (float)x : (float)y : (float)z
{
    
    
    if (g_gyro_need_reload)
    {
        g_gyro_need_reload = NO;
        z_move_start = z;
        center_gyro = m_circleBack.center;
        return;
    }
    
    
    double z2 = z - z_move_start;
    
    
    bool draw_green = NO;
    
    //if (z1 < 0.6 && z1 > -0.6)
    if(((z_move_start + rightGyroBorder) <= 3.14159) && ((z_move_start - rightGyroBorder) >= -3.14159))
    {
        if (z2 < rightGyroBorder && z2 > leftGyroBorder)
        draw_green = YES;
    }
    else if((z_move_start + rightGyroBorder) >= 3.14159)
    {
        double zm = -3.14159 + rightGyroBorder - (3.14159 - z_move_start);
        if((z >= -3.14159 && z < zm) || (z > (z_move_start - rightGyroBorder) && z <= 3.14159))
        draw_green = YES;
    }
    else if((z_move_start - rightGyroBorder) < -3.14159)
    {
        double zm = 3.14159 - rightGyroBorder - (-3.14159 - z_move_start);
        if((z >= -3.14159 && z < (z_move_start + rightGyroBorder)) || (z > zm && z <= 3.14159))
        draw_green = YES;
    }
    m_point_Gyro.hidden = NO;
    if(draw_green)
    {
        center_gyro.x = m_circleBack.center.x + 42*cos(z2 - (3.1415926*0.5));
        center_gyro.y = m_circleBack.center.y + 42*sin(z2 - (3.1415926*0.5));
        [m_point_Gyro setCenter:center_gyro];
        m_point_Gyro.image = [UIImage imageNamed:@"RadarAssetYellowPoint"];
        gyroOutOfRange = NO;
    }
    else
    {
        [m_point_Gyro setCenter:center_gyro];
        m_point_Gyro.image = [UIImage imageNamed:@"RadarAssetRedPoint"];
        gyroOutOfRange = YES;
    }
    
}

-(void) refreshGyroStateData
{
    if (self.delegate)
        [self.delegate setGyroOutOfRange:acelOutOfRange || gyroOutOfRange];
}

@end
