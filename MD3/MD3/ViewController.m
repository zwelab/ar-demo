

#import "ViewController.h"
#import "GroundScan3dSettingsViewController.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>



@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@end

@implementation ViewController

static CLLocationManager* locationManager = [[CLLocationManager alloc] init];
static CLLocation* currentLocation = [[CLLocation alloc] init];

- (void)viewDidLoad
{
    [super viewDidLoad];
    calCount = 5;
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
}

- (int)getCalCount
{
    return self->calCount;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             //NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             //NSString *Address = [[NSString alloc]initWithString:locatedAt];
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSString *Country = [[NSString alloc]initWithString:placemark.country];
             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
             g_currentLocation = CountryArea;
             g_coord = placemark.location.coordinate;
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
         }
     }];
}

+(NSString*) currentLocation
{
    if ([GroundScan3dSettingsViewController getValue: @"gpsLocation"] == 0)
        return g_currentLocation;
    else
        return @"";
}

+(CLLocationCoordinate2D) currentCoordinate
{
    return g_coord;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)prodWebPageButton:(UIButton *)sender {
}


- (IBAction)pressGroundScan:(id)sender {
    [self performSegueWithIdentifier:@"ARSegue" sender:nil];
      
}
@end
