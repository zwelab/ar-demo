
#import <UIKit/UIKit.h>
#import "UILLable.h"
#import "UIBatteryView.h"


@interface GroundScan3dSettingsViewController : UIViewController{
    NSTimer *connectionStateTimer;
    UIAlertView *alert;

    bool notBalancedShow;
    bool notConnectedShow;
}

+ (NSInteger)getValue:(NSString*) key;
+ (void)setValue:(NSString*)key :(NSInteger)value;
+ (void)setValue:(NSString *)key float:(float)value;
+ (float)getValueFloat:(NSString *)key;
+ (void)shareThisScreen:(UIViewController*)view;
+ (void)shareCSV:(UIViewController*)view withData:(NSString*)data;
+(bool) CheckSupportARKit;
+(bool) CheckSupportOnlyARKit;

- (void)disableDirectionAndLengthSettings:(BOOL)disable;

@property (weak, nonatomic) IBOutlet UISegmentedControl *sensitivitySegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scanModeSegmentedContol;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scanDirectionSegmentedControl;

@property (weak, nonatomic) IBOutlet UILLabel *detectorVersionLabel;
@property (weak, nonatomic) IBOutlet UILLabel *deviceInfoLabel;
@property (weak, nonatomic) IBOutlet UILLabel *lengthLabel;
@property (weak, nonatomic) IBOutlet UILLabel *volumeLabel;

@property (weak, nonatomic) IBOutlet UILLabel *gridSizeLabel;

@property (weak, nonatomic) IBOutlet UIStepper *lengthStepper;
@property (weak, nonatomic) IBOutlet UIStepper *volumeStepper;
@property (weak, nonatomic) IBOutlet UIStepper *gridSizeStepper;
@property (weak, nonatomic) IBOutlet UISegmentedControl *phonePosition;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gpsLocation;
@property (weak, nonatomic) IBOutlet UIBatteryView *batteryView;

@end

