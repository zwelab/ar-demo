
#import "GroundScan3dSettingsViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIGraphics.h>
#import <ARkit/ARKit.h>
#import "defines.h"

@interface GroundScan3dSettingsViewController (){
    NSArray *gridSizeValues;
}

@end

@implementation GroundScan3dSettingsViewController


enum TAGS {
    BUY_TAG = 1,
    BALANCED_TAG,
    CAMERA_MODE,
    PHONE_POSITION,
    OLD_FIRMWARE
};

int lastMode;
int lastPosition;

-(void)checkConnectionState
{
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    notBalancedShow = NO;
    notConnectedShow = NO;
    gridSizeValues = @[@1,@2,@5,@10,@20,@50];
    // Detector device
    
    // http://www.codigator.com/snippets/nsuserdefaults-tutorial-for-ios/
    NSInteger intValue;
    
    if([GroundScan3dSettingsViewController getValue:@"firstRun"] != 200)
    {
        [GroundScan3dSettingsViewController setValue:@"sensitivity": 2];
        [GroundScan3dSettingsViewController setValue:@"phonePosition": 0];
        [GroundScan3dSettingsViewController setValue:@"gpsLocation": 1];
        [GroundScan3dSettingsViewController setValue:@"scanMode": 1];
        [GroundScan3dSettingsViewController setValue:@"useARKit": 1];
        [GroundScan3dSettingsViewController setValue:@"scanLen": 1];
        lastMode = 1;
        lastPosition = 0;
        [GroundScan3dSettingsViewController setValue:@"volume": 50];
        [GroundScan3dSettingsViewController setValue:@"scanDir": 1];
        [GroundScan3dSettingsViewController setValue:@"firstRun": 200];
        [GroundScan3dSettingsViewController setValue:@"arGridSize": 1];
        [GroundScan3dSettingsViewController setValue:@"arOpacity": 50000];
        
    }
    
    // Default settings
    
    intValue = [GroundScan3dSettingsViewController getValue:@"arGridSize"];
    for (NSInteger i = 0 ;i < [gridSizeValues count];i++)
    {
        NSNumber *number = (NSNumber*)[gridSizeValues objectAtIndex:i];
        
        if ([number intValue] == intValue)
        {
            _gridSizeStepper.value = i ;
            [_gridSizeLabel setText:[NSString stringWithFormat:[[NSBundle mainBundle] localizedStringForKey:_gridSizeLabel.lzText value:@"" table:nil], ((float)intValue)/100]];
            
        }
    }
    // sensitivity
    _sensitivitySegmentedControl.selectedSegmentIndex=[GroundScan3dSettingsViewController getValue:@"sensitivity"];
    // scan mode
    _scanModeSegmentedContol.selectedSegmentIndex=[GroundScan3dSettingsViewController getValue:@"scanMode"];
    lastMode = (int)[GroundScan3dSettingsViewController getValue:@"scanMode"]; //_scanModeSegmentedContol.selectedSegmentIndex;
 //   [self disableDirectionAndLengthSettings:[GroundScan3dSettingsViewController getValue:@"scanMode"] == 2];
 //   [self disableGridSize:[GroundScan3dSettingsViewController getValue:@"scanMode"] != 2];
    
    // scan len
    _lengthStepper.maximumValue = 20;
    _lengthStepper.minimumValue = 1;
    _lengthStepper.value=[GroundScan3dSettingsViewController getValue:@"scanLen"];
    double value = _lengthStepper.value;
    [_lengthLabel setText:[NSString stringWithFormat:[[NSBundle mainBundle] localizedStringForKey:@"MDLength" value:@"" table:nil], (int)value]];
    // scan dir
    _scanDirectionSegmentedControl.selectedSegmentIndex=[GroundScan3dSettingsViewController getValue:@"scanDir"];
    // volume (stepper goes from 0-10, volume from 0-100)
    intValue = [GroundScan3dSettingsViewController getValue:@"volume"];
    _volumeStepper.maximumValue = 100;
    _volumeStepper.minimumValue = 0;
    _volumeStepper.stepValue = 10;
    _volumeStepper.value=intValue;
    [_volumeLabel setText:[NSString stringWithFormat:[[NSBundle mainBundle] localizedStringForKey:@"MDVolume" value:@"" table:nil], (int)_volumeStepper.value]];
    
  


    
    _phonePosition.selectedSegmentIndex = [GroundScan3dSettingsViewController getValue:@"phonePosition"];
    lastPosition = (int)[GroundScan3dSettingsViewController getValue:@"phonePosition"];
    _gpsLocation.selectedSegmentIndex = [GroundScan3dSettingsViewController getValue:@"gpsLocation"];


}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

+ (NSInteger)getValue:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey: key];
}

+ (float)getValueFloat:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults floatForKey: key];
}

+ (void)setValue:(NSString *)key :(NSInteger)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
}

+ (void)setValue:(NSString *)key float:(float)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:value forKey:key];
    [defaults synchronize];
}


- (void)disableDirectionAndLengthSettings:(BOOL)disable
{
    float alpha = disable == YES ? 0.2 : 1;
    _scanDirectionSegmentedControl.userInteractionEnabled = !disable;
    _lengthStepper.userInteractionEnabled = !disable;

    _scanDirectionSegmentedControl.alpha = alpha;
    _lengthStepper.alpha = alpha;
   
}

- (void)disableGridSize:(BOOL)disable
{
    if(![GroundScan3dSettingsViewController CheckSupportOnlyARKit])
    {
        disable = YES;
    }
    
    float alpha = disable == YES ? 0.2 : 1;
    
    _gridSizeStepper.userInteractionEnabled = !disable;
    _gridSizeStepper.alpha = alpha;
}


- (IBAction)gpsLocationChange:(UISegmentedControl*)sender {
    int value = (int)sender.selectedSegmentIndex;
    [GroundScan3dSettingsViewController setValue: @"gpsLocation": value];
}

+(bool) CheckSupportARKit
{
    return YES;
}

+(bool) CheckSupportOnlyARKit
{
    if (@available(iOS 11.0, *)) {
        BOOL isSupport = [ARConfiguration isSupported];
        if (isSupport)
        {
            return YES;
        }
        return NO;
    } else {
        return NO;
    }
}

@end
