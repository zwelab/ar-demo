

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "defines.h"


static NSString* g_currentLocation;
static CLLocationCoordinate2D g_coord;

@interface ViewController : UIViewController<CLLocationManagerDelegate>{
    int calCount;
}
+(NSString*) currentLocation;
+(CLLocationCoordinate2D) currentCoordinate;

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
@property (weak, nonatomic) IBOutlet UIButton *calButton;
@property(nonatomic) BOOL isCalMode;
- (IBAction)calButtonPressed:(id)sender;
- (int)getCalCount;

@end
