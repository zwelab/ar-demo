

#ifndef UIARMainViewControllerDelegate_h
#define UIARMainViewControllerDelegate_h

@protocol ARMainViewControllerDelegate

-(void) dismissViewController;
-(void) resetARstate;
-(void) setDrawGridMode:(BOOL)_draw;
-(void) sharePress;
-(void) savePress;
-(BOOL) searchGrid:(BOOL)_value;
-(void) show3DMap;
-(void) setOpacityTexture:(float)_value;
-(void) setGyroOutOfRange:(BOOL)_value;
-(void) setDoubleTapMove:(int) state :(CGPoint)_position;
@end

@protocol ARCircleLogicDelegate <NSObject>
-(void) circleLogicSetNormalState;
-(void) circleLogicSetWaitState;
-(void) circleLogicChangeColor:(UIColor*) _color;
-(UIColor*) circleLogicGetColor;
-(void) circleLogicAddPulse;
-(void) circleLogicRemovePulse;
-(void) circleLogicLockColor;
-(void) circleLogicUnlockColor;
@end

#endif /* UIARMainViewControllerDelegate_h */
