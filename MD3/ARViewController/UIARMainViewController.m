

#import "UIARMainViewController.h"
#import "ViewController.h"
#import "GroundScan3dSettingsViewController.h"


enum TAGS {
    BUY_TAG = 1,
    BALANCED_TAG,
    FILE_SAVE_TAG,
    REWRITE_TAG,
    SHARE_TAG,
    DEPTH_TAG
};

@interface UIARMainViewController() {
    float magOld;
    float magNew;
}

@end

@implementation UIARMainViewController

static BOOL g_gyro_need_reload = NO;
static BOOL g_accel_need_reload = NO;
static BOOL g_can_search_grid = NO;
static BOOL g_last_state_search_button = NO;
static BOOL g_show3dGraph = NO;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder: aDecoder])
    {
        arview = [[UIARViewController alloc] init];
   
        self.interfaceView  = [[ARInterfaceView alloc] init];
        [self.interfaceView.opacitySlider.slider setValue:[arview getTextureAlpha]];
        arview.delegateCircle = self.interfaceView;
        
        m_current_state = ARTrackingStateNotAvailable;
        [self.interfaceView.label setText:@""];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

   //  [sharedDetectorDevice modeSet:devModeSimulator];
    
    g_can_search_grid = NO;
    magOld=0;
    magNew=0;
    

    [self.interfaceView.balanceLabel setText:@""];
    [self checkConnectionState];
    
    connectionStateTimer = [NSTimer scheduledTimerWithTimeInterval:5
                                                            target:self
                                                          selector:@selector(checkConnectionState)
                                                          userInfo:nil
                                                           repeats:YES];
    
    dataTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                            target:self
                                                          selector:@selector(generateData)
                                                          userInfo:nil
                                                           repeats:YES];
    
    // Setup AR view
    arview.delegate = self;
    [self.view setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:arview.view];
    
    self.interfaceView.frame =CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
    self.interfaceView.delegate = self;
    [self.view addSubview:self.interfaceView];
    
    [self loadCirclePosition];
    
    
}

-(void) loadCirclePosition{
    CGPoint m_location = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
    float loc_x = [GroundScan3dSettingsViewController getValueFloat:@"m_locationPosX"];
    float loc_y = [GroundScan3dSettingsViewController getValueFloat:@"m_locationPosY"];
    if (loc_x != 0 && loc_y != 0)
    {
        m_location.x = loc_x;
        m_location.y = loc_y;
    }
    [self setDoubleTapMove:4 :m_location];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    if (g_show3dGraph!= YES)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                 target:self
                                               selector:@selector(refreshGraphData)
                                               userInfo:nil
                                                repeats:YES];
    }
    g_show3dGraph = NO;
    
    [self.interfaceView startMotionManager];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.interfaceView stopMotionManager];
    [self.interfaceView willDisappear];
    if (g_show3dGraph!= YES)
    {
        
        [timer invalidate];
        [connectionStateTimer invalidate];
        
    }
}

-(void) errorARView:(NSString*)_error :(ARViewErrorCodes)_code
{
    //NSLog(@"%@",_error);
}

-(void) trackingState:(NSString*)_str :(ARTrackingState)_state :(int)_code
{
    switch(_state)
    {
        case ARTrackingStateNormal:
           // [self.interfaceView.label setText:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackStateNormal" value:@"" table:nil]];
            if (!notBalancedShow && m_IsPlanFounded)
                 [self showMessage:@"Press multi-functional button to balance the detector" :[UIColor redColor]];
            else
                 [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackStateSearchPlan" value:@"" table:nil] :[UIColor redColor]];

   

                
            if (m_current_state == ARTrackingStateLimited && g_last_state_search_button)
            {
               self.interfaceView.searchButton.selected= [self searchGrid:YES];
                g_last_state_search_button = NO;
            }
            
            break;
        case ARTrackingStateNotAvailable:
            g_can_search_grid = NO;
            m_IsPlanFounded = NO;
            [self resetSearchButtonState];
           
            break;
        case ARTrackingStateLimited:
        {

            
            [self.interfaceView.label setTextColor:[UIColor redColor]];
            g_can_search_grid = NO;
            m_IsPlanFounded = NO;
            g_last_state_search_button = self.interfaceView.searchButton.selected;
            [self resetSearchButtonState];
            
            switch (_code) {
                case ARTrackingStateReasonInitializing:
                    [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackStateLimitedInitialization" value:@"" table:nil] :[UIColor redColor]];
                    break;
                case ARTrackingStateReasonRelocalizing:
                    [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackStateLimited" value:@"" table:nil] :[UIColor redColor]];
                    break;
                case ARTrackingStateReasonExcessiveMotion:
                    [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackingStateReasonExcessiveMotion" value:@"" table:nil] :[UIColor redColor]];
                    break;
                case ARTrackingStateReasonInsufficientFeatures:
                    [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackingStateReasonInsufficientFeatures" value:@"" table:nil] :[UIColor redColor]];
                    break;
                default:
                    [self showMessage:@"" :[UIColor redColor]];
                    break;
            }
            
        }
            break;
        default:
            break;
    }
     m_current_state = _state;
}


-(void) dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)generateData
{
    [self NewDataFromDevice];
}


-(void)checkConnectionState
{
    g_can_search_grid = YES;
    [arview SetBalanceState:YES];
    [arview SetConnectedState:YES];
    
    g_gyro_need_reload = YES;
    g_accel_need_reload = YES;
    notBalancedShow = YES;
    g_can_search_grid = YES;
    [self.interfaceView.balanceLabel setText:@""];
    [self setBalanceGuroInView];
    [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackStateSearchPlan" value:@"" table:nil]:[UIColor redColor]];
    self.interfaceView.searchButton.selected= [self searchGrid:!self.interfaceView.searchButton.selected];
    [self searchGrid:YES];
}


-(void)refreshGraphData {
}

- (void)NewDataFromDevice
{

   [_interfaceView.batteryView setBatterylevel:0.0f];
    float fval=((float)rand() / RAND_MAX) * 2 - 1;
        if(fval>1.0)
            fval=1.0;
        if(fval<-1.0)
            fval=-1.0;
        magNew=fval;
}

-(NSNumber*) drawGridNodeWithValue
{
    return [NSNumber numberWithFloat:magNew];
}

-(void) resetARstate
{
    notBalancedShow = NO;
    g_can_search_grid = NO;
    [arview SetBalanceState:NO];
    g_gyro_need_reload = NO;
    g_accel_need_reload = NO;
    [self resetSearchButtonState];
    [arview resetAR];
}

-(void) resetSearchButtonState
{
    [self searchGrid:NO];
    [self.interfaceView.searchButton setSelected:NO];
}

-(void) setDrawGridMode:(BOOL)_draw
{
    [arview setDrawGridMode:_draw];
}

-(void) setDoubleTapMove:(int) state :(CGPoint)_position{
    if ([self.interfaceView moveCircle:state :_position])
        [arview moveCircle:state :_position];
}

-(BOOL) searchGrid:(BOOL)_value
{
    BOOL beginSearch = YES;
    
    if (g_can_search_grid && _value)
        [arview beginDrawGrid];
    else
    {
        [arview endDrawGrid];
        beginSearch = NO;
    }
    return beginSearch;
}

-(void) planIsFound:(BOOL)_value
{
    m_IsPlanFounded = _value;
    if (!_value)
        [self showMessage:[[NSBundle mainBundle] localizedStringForKey:@"ARTrackStateSearchPlan" value:@"" table:nil] :[UIColor redColor]];
   
}

-(void) showMessage:(NSString*)_message :(UIColor*)_color;
{
    if(notConnectedShow)
    {
        [self.interfaceView.label setText:@""];
        return;
    }
    
    [self.interfaceView.label setText:_message];
    [self.interfaceView.label setTextColor:_color];
   
}

- (BOOL)shouldAutorotate {
    return NO;
}

-(void) setOpacityTexture:(float)_value
{
    [arview setTextureAlpha:_value];
}

-(void) setGyroOutOfRange:(BOOL)_value
{
    [arview setGyroOutOfRange:_value];
}

-(void) setBalanceGuroInView
{
    [self.interfaceView startCalibration];
}
@end
