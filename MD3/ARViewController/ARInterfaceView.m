
#import <CoreMotion/CoreMotion.h>

#import "ARInterfaceView.h"
#import "ViewController.h"
#import "GroundScan3dSettingsViewController.h"

enum TAGS {
    FILE_SAVE_TAG = 1,
    REWRITE_TAG,
    BUY_TAG,
    BALANCED_TAG,
    SHARE_TAG,
    DEPTH_TAG
};




@implementation ARInterfaceView{
    __weak IBOutlet UIButton *gridButton;
}


-(id) init
{
    if (self = [super init])
    {
       

        [self startCalibration];
        
        [[UINib nibWithNibName:@"ARInterfaceView" bundle:nil] instantiateWithOwner:self options:nil];
        [self addSubview:self.view];
        self.view.frame = self.bounds;
     
        [self.label setAdjustsFontSizeToFitWidth:YES];

        
        [_opacitySlider.slider addTarget:self
                      action:@selector(sliderDidEndSliding:)
            forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
        

        UIPanGestureRecognizer *tapGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [tapGesture setMaximumNumberOfTouches:1];
        [tapGesture setMinimumNumberOfTouches:1];
        [self.view addGestureRecognizer:tapGesture];
        
        
        [_curcleView setBackgroundColor:[UIColor clearColor]];
        m_circle = [CircleNode2 new];
        _curcleView.scene = [m_circle getScane];
        _curcleView.pointOfView = [m_circle getCamera];
        _curcleView.delegate = m_circle;
        m_circle.aim_test = _aimimage;
        [m_circle startMotionManager];
        self.m_radar.delegate = self;
        
    }
    return self;
}


-(void) willDisappear{
    [m_circle saveLocation];
}

-(bool) moveCircle:(int) state :(CGPoint)_position{
    return [m_circle moveCircle:state :_position];
}

- (void)setBounds:(CGRect)bounds{
    [super setBounds:bounds];

}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [m_circle setScreenSize:frame.size.width :frame.size.height];
}

- (void)handleTouchGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
      
    }
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    CGPoint location = [sender locationInView:self];
    if (sender.state == UIGestureRecognizerStateEnded || sender.state == UIGestureRecognizerStateFailed || sender.state == UIGestureRecognizerStateCancelled) {
        [self.delegate setDoubleTapMove:3:location ];
    }
    else
        if (sender.state == UIGestureRecognizerStateBegan)
            [self.delegate setDoubleTapMove:1: location];
        else
            [self.delegate setDoubleTapMove:2: location];
}

-(void) setGyroOutOfRange:(int) _state;
{
    [self.delegate setGyroOutOfRange:_state];
}

-(void) startCalibration
{
    [self.m_radar resetPosition];
}



-(void) startMotionManager{
    [self.m_radar startMotionManager];
}

-(void) stopMotionManager
{
    [self.m_radar stopMotionManager];
}

- (void)dealloc
{
 

}


- (void)sliderDidEndSliding:(NSNotification *)notification {
    if (self.delegate) {
        [self.delegate setOpacityTexture:_opacitySlider.slider.value];
    }
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [[UINib nibWithNibName:@"ARInterfaceView" bundle:nil] instantiateWithOwner:self options:nil];
        [self addSubview:self.view];
        self.view.frame = self.bounds;
    }
    return self;
}
- (IBAction)pressExit:(id)sender {
   
    if (self.delegate) {
        [self.delegate dismissViewController];
    }
    
}
- (IBAction)resetButton:(id)sender {
    if (self.delegate) {
        [self.delegate resetARstate];
    }
}
- (IBAction)gridButtonToggle:(id)sender {
    gridButton.selected = !gridButton.selected;
    if (gridButton.selected)
    {
        gridButton.alpha = 1;
    }
    else
    {
        gridButton.alpha = 0.3;
    }
    
    if (self.delegate) {
        [self.delegate setDrawGridMode:gridButton.selected];
    }
}
- (IBAction)sharedPress:(id)sender {
    if (self.delegate) {
        [self.delegate sharePress];
    }
}
- (IBAction)savePress:(id)sender {
    if (self.delegate) {
        [self.delegate savePress];
    }
}
- (IBAction)searchButton:(id)sender {
    if (self.delegate) {
        BOOL state = [self.delegate searchGrid:!self.searchButton.selected];
        self.searchButton.selected = state;
     
    }
}

-(void) setSearchViewState:(BOOL)_visible :(NSString*)_text
{
    searchPlaneView.hidden = !_visible;
    [searchPlaneViewLabel setText:_text];
}
    
- (IBAction)scan3DShow:(id)sender {
    if (self.delegate) {
        [self.delegate show3DMap];
    }

}

-(void) circleLogicSetNormalState{
    [m_circle SetNormalState];
}

-(void) circleLogicSetWaitState{
    [m_circle SetWaitState];
}

-(void) circleLogicChangeColor:(UIColor*) _color{
    [m_circle ChangeColor:_color];
}

-(UIColor*) circleLogicGetColor{
    return [m_circle getColor];
}

-(void) circleLogicAddPulse{
    [m_circle AddPulse];
}

-(void) circleLogicRemovePulse{
    [m_circle RemovePulse];
}

-(void) circleLogicLockColor{
    [m_circle LockColor];
}

-(void) circleLogicUnlockColor{
    [m_circle UnlockColor];
}

@end


