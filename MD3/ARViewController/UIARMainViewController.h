

#import <UIKit/UIKit.h>

#import "UIARViewController.h"
#import "ARInterfaceView.h"
#import "UIARMainViewControllerDelegate.h"

API_AVAILABLE(ios(11.0))
@interface UIARMainViewController : UIViewController<ARViewProtocol,ARMainViewControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>
{
    NSTimer *timer;
    NSTimer *connectionStateTimer;
    NSTimer *dataTimer;

    UIAlertView *alert;
    bool notConnectedShow;
    bool notBalancedShow;
    bool m_IsPlanFounded;
    UIARViewController *arview;
    ARTrackingState m_current_state;
    UITextField *passwordTextField;
    NSString *fileName;
    UIImage *currentImage;
}
@property (nonatomic, strong) ARInterfaceView *interfaceView;
-(void) dismissViewController;
-(void) resetARstate;
-(void) setDrawGridMode:(BOOL)_draw;
-(void) show3DMap;
-(void) setOpacityTexture:(float)_value;
-(void) setGyroOutOfRange:(BOOL)_value;
@end

