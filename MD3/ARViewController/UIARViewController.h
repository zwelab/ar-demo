

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>
#import "UIARMainViewControllerDelegate.h"
#import "UIARTextureHelper.h"
#import "UIARViewDefs.h"
#import "Objects/ARPlaneGrid.h"
#import "CircleNode.h"

@protocol ARViewProtocol;

API_AVAILABLE(ios(11.0))
@interface UIARViewController :UIViewController < ARSCNViewDelegate,ARSessionDelegate> {
@private
    NSThread *aThread;
    BOOL m_drawGrid;
 //   CircleNode *circle;
    CGPoint m_location;
    long long           m_move_time;
    double              m_current_move_time;
    long long           m_start_time;
    CGPoint             m_move_vector_location;
    CGPoint             m_start_location;
    bool                m_animationMove;
    UIColor*            m_currentCircleColor;
    bool                m_isResetAR;

  
@protected
    ARPlaneGrid* m_plans;
    CGSize m_sizeCell;
    
}
@property (nonatomic, weak) id<ARViewProtocol> delegate;
@property (nonatomic, strong) ARSCNView *sceneView;
@property (nonatomic) BOOL show3Dgraph;
@property (nonatomic, weak) id<ARCircleLogicDelegate> delegateCircle;

@property (nonatomic, readonly) CGSize sizeCell;

-(id) init;
-(void) beginDrawGrid;
-(void) endDrawGrid;
-(void) setSizeCell:(CGSize)_sizeCell;
-(void) clearGrid;
-(void) resetAR;
-(void) setDrawGridMode:(BOOL)_drawGrid;
-(float*) generateDataForCSV:(int*) _width :(int*) _height;
-(float*) generateDataForCSV:(int*) _width :(int*) _height :(float) _defaultValue;
-(BOOL) GeneratePoint;
-(void) ChangeCircleColor:(UIColor*)_color;
-(void) RemovePulse;
-(void) SetBalanceState:(BOOL)_state;
-(void) SetConnectedState:(BOOL)_state;
-(float) getTextureAlpha;
-(void)  setTextureAlpha:(float) _value;
-(void) setGyroOutOfRange:(BOOL)_value;
-(void) moveCircle:(int) state :(CGPoint)_position;

@end

API_AVAILABLE(ios(11.0))
@protocol ARViewProtocol<NSObject>

@optional
-(void) errorARView:(NSString*)_error :(ARViewErrorCodes)_code;
-(void) trackingState:(NSString*)_str :(ARTrackingState)_state :(int)_code;
-(void) beginDraw;
-(void) endDraw;
// Must get value in _val in range -1 ... 1
-(NSNumber*) drawGridNodeWithValue;
-(void) planIsFound:(BOOL)_value;
-(void) showMessage:(NSString*)_message :(UIColor*)_color;
-(void) show3DMap;
-(void) resetARstate;

@end
