
#import <ARKit/ARKit.h>
#import <Foundation/Foundation.h>

#import "Objects/ARPlaneGrid.h"

API_AVAILABLE(ios(11.0))
@interface UIARTextureHelper : NSObject

+(void) ConfiguratePlane:(ARPlaneGrid*)_grid :(ARPlaneGridItem*)_plane :(CGFloat)_magvalue :(CGFloat) _alpha;
+ (UIImage *) convertBitmapRGBA8ToUIImage:(unsigned char *) buffer
                                withWidth:(int) width
                               withHeight:(int) height;

@end
