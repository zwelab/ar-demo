

#import "UIARViewController.h"
#import "ARFloor.h"
#import "ARPlane.h"
#import "UIARkitExtends.h"
#import "GroundScan3dSettingsViewController.h"
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>


@implementation UIARViewController
@synthesize  sizeCell = m_sizeCell;
BOOL pressTap = NO;
ARTrackingState g_state; // Check state AR kit
BOOL g_balanceState;
BOOL g_connected;
float g_texturealpha;
BOOL g_gyroOutOfRange;

CGPoint g_screenSize;
//// Debug
SCNNode *boxNode;
SCNNode *boxNode2;
////

-(id) init
{
    if ( self = [super init] ) {
        self.sceneView = [ARSCNView new];
        if (!self.sceneView)
            return nil;
        self.sceneView.frame =CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:self.sceneView];
        self.sceneView.pointOfView.camera.zNear = 0.0005;


        float size =  20; // Configurable size of the grid
        
        g_texturealpha = 0.5; // Opacity
        m_move_time = 200;
        m_sizeCell = CGSizeMake(size/100, size/100);
        m_plans = [[ARPlaneGrid alloc] init];
        m_drawGrid = NO;
        [m_plans SetDrawModeForAllPlane:m_drawGrid];

        g_connected = NO;
        self.show3Dgraph = NO;
        
        
        
//        m_location = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
//        float loc_x = [GroundScan3dSettingsViewController getValueFloat:@"m_locationPosX"];
//        float loc_y = [GroundScan3dSettingsViewController getValueFloat:@"m_locationPosY"];
//
//        if (loc_x != 0 && loc_y != 0)
//        {
//            m_location.x = loc_x;
//            m_location.y = loc_y;
//        }
//
//        g_screenSize.x =self.view.frame.size.width/2.0f;
//        g_screenSize.y =self.view.frame.size.height/2.0f;
        
        
        
//        SCNBox *box = [SCNBox boxWithWidth:0.005 height:0.005 length:0.005 chamferRadius:0.001];
//        box.materials.firstObject.diffuse.contents = [UIColor blueColor];
//        boxNode = [SCNNode new];
//        boxNode.geometry = box;
//        m_plans.boxNode = boxNode;
//        [self.sceneView.scene.rootNode addChildNode:boxNode];
//        
//
//        SCNBox *box2 = [SCNBox boxWithWidth:0.005 height:0.005 length:0.005 chamferRadius:0.001];
//        box2.materials.firstObject.diffuse.contents = [UIColor whiteColor];
//        boxNode2 = [SCNNode new];
//        boxNode2.geometry = box2;
//        m_plans.boxNode2 = boxNode2;
//        [self.sceneView.scene.rootNode addChildNode:boxNode2];

    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sceneView.delegate = self;
    self.sceneView.showsStatistics = NO;
    self.sceneView.autoenablesDefaultLighting = false;
    self.sceneView.automaticallyUpdatesLighting = false;
 //   self.sceneView.debugOptions = (ARSCNDebugOptionShowWorldOrigin| ARSCNDebugOptionShowFeaturePoints);
 //   self.sceneView.debugOptions =  ARSCNDebugOptionShowFeaturePoints;
    self.sceneView.antialiasingMode = SCNAntialiasingModeMultisampling4X;
}

-(void)threadLoop
{
    
    while([[NSThread currentThread] isCancelled] == NO)
    {
        [NSThread sleepForTimeInterval:0.05];
           @synchronized(self)
            {
                [self performSelectorOnMainThread:@selector(GeneratePointAndDelegate) withObject:nil waitUntilDone:YES];
            }
    }
}



- (void)viewWillAppear:(BOOL)animated {
    if (self.show3Dgraph!= YES)
    {
        [super viewWillAppear:animated];
       
        if (!ARWorldTrackingConfiguration.isSupported)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(errorARView::)]) {
                [self.delegate errorARView:@"Error: World tracking is not supported":ARErrorWorldTrackingIsNotSupport];
                return;
            }
        }
        ARWorldTrackingConfiguration *configuration = [[ARWorldTrackingConfiguration alloc] init];
        configuration.planeDetection = ARPlaneDetectionHorizontal ;
        configuration.lightEstimationEnabled  = YES;
        [self.sceneView.session runWithConfiguration:configuration];
        
        aThread = [[NSThread alloc] initWithTarget:self selector:@selector(threadLoop) object:nil];
        [aThread start];

        [self RemoveWaitCircle];
    }
    self.show3Dgraph = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.show3Dgraph!= YES)
    {
        [super viewWillDisappear:animated];
    
        [self.sceneView.session pause];
        [aThread cancel];
    }
    

}
#pragma mark - ARSCNViewDelegate




-(void) session:(ARSession *)session didUpdateFrame:(ARFrame *)frame
{
  
}

// SCNMatrix4 cameraPosLast;



-(void) renderer:(id<SCNSceneRenderer>)renderer updateAtTime:(NSTimeInterval)time
{
//    @synchronized(circle)
//    {
//    
//        ///
//        ARFrame *frame = self.sceneView.session.currentFrame;
//        ///
//        simd_float4x4 projectMatrix = frame.camera.projectionMatrix;
//        simd_float4x4 cameraTransform =  frame.camera.transform;
//        SCNMatrix4 camTrans =  SCNMatrix4FromMat4(cameraTransform);
//        SCNVector3 nearVec = SCNVector3Make(m_location.x, m_location.y, 0);
//        SCNVector3 farVec  = SCNVector3Make(m_location.x, m_location.y, 1);
//        SCNMatrix4 matview = SCNMatrix4FromMat4(cameraTransform);
//        
//        SCNVector3 nearPoint = [self.sceneView unprojectPoint:nearVec];
//        SCNVector3 farPoint = [self.sceneView unprojectPoint:farVec];
//        SCNVector3 viewVec = SCNVector3Make(farPoint.x-nearPoint.x, farPoint.y-nearPoint.y, farPoint.z-nearPoint.z);
//        double vectorLength = sqrt(viewVec.x*viewVec.x + viewVec.y*viewVec.y + viewVec.z*viewVec.z);
//        SCNVector3 normalizedViewVector = SCNVector3Make(viewVec.x/vectorLength, viewVec.y/vectorLength, viewVec.z/vectorLength);
//        float Scale = 15;
//        SCNVector3 LockCameraDirection = SCNVector3Make(normalizedViewVector.x * Scale, normalizedViewVector.y * Scale, normalizedViewVector.z * Scale);
//        
//       // LockCameraDirection = [MathHelpers MulVecMat:LockCameraDirection :matview];
//   
//        
//        
//        //SCNVector3 LockCameraDirection = SCNVector3Make(-1 * cameraTransform.columns[2][0] ,  -1 * cameraTransform.columns[2][1], -1 * cameraTransform.columns[2][2]);
//        
//      //  [self.sceneView unprojectPoint:<#(CGPoint)#> ontoPlaneWithTransform:<#(simd_float4x4)#>]
//        ///
////
// //       SCNVector3 locationCamera =  self.sceneView.pointOfView.position;
////        SCNVector3 orientation = SCNVector3Make(-camTrans.m31, -camTrans.m32, camTrans.m33);
////        locationCamera = [MathHelpers Sum:orientation: locationCamera];
//   //     nearPoint = [MathHelpers Sub:nearPoint :locationCamera];
//       
//     //   NSLog(@"Dir %f %f %f",nearPoint.x,nearPoint.y,nearPoint.z);
//        
//
//        
//        
//        SCNVector3 vec;
//        vec.x = 0;
//        vec.y = 0;
//        vec.z = 0;
//        
//        
// 
//        
//        
//        if (frame == nil)
//            return;
//        CGSize cameraImageSize =   frame.camera.imageResolution;
//       
//        CGSize size;
//        size.width = g_screenSize.x * 2;
//        size.height = g_screenSize.y * 2;
//        
//        simd_float4x4 viewm =  [frame.camera projectionMatrixForOrientation:UIInterfaceOrientationLandscapeLeft viewportSize:cameraImageSize zNear:0.001 zFar:1000];
//        simd_float3x3 intrinsics =  frame.camera.intrinsics;
//        
//        
//        
//        float yScale = viewm.columns[1][1];
//        float yFovScreen = 2 * atan(1/yScale) * 180/M_PI;
//        float xFovScreen = yFovScreen * (size.height/size.width);
//        float xFovCamera = 2 * atan(cameraImageSize.width/(2.0 * intrinsics.columns[0][0])) * 180 / M_PI;
//        float yFovCamera = 2 * atan(cameraImageSize.height/(2.0 * intrinsics.columns[1][1])) * 180 / M_PI;
//        
//         NSLog(@"FOV cam %f %f",xFovCamera,yFovCamera);
//        
//        float magic_kof1 = xFovCamera / 62.8 ;
//        float magic_kof2 = yFovCamera / 37.9 ;
//
//        magic_kof1 =1;
//        magic_kof2 =1;
//        
//        viewm = [frame.camera projectionMatrix];
//        SCNMatrix4 prjT = self.sceneView.pointOfView.camera.projectionTransform;
//   //     self.sceneView.pointOfView.camera.projectionDirection
//        yScale = prjT.m22;
//        yFovScreen = 2 * atan(1/yScale) * 180/M_PI;
//        xFovScreen = yFovScreen * (size.height/size.width);
//        NSLog(@"FOV screen %f %f %f",xFovScreen,yFovScreen,self.sceneView.pointOfView.scale);
//        
//   //     float ScreenSizeY = viewm.columns[1][1] * 0.0001 * magic_kof1;
//   //     float ScreenSizeX = viewm.columns[0][0] * 0.0001 * magic_kof2;
// 
//        float Kof = yFovScreen / self.sceneView.pointOfView.camera.fieldOfView  ;
//        Kof = 1 ;
//        float ScreenSizeY = prjT.m11   * 0.0002 * Kof ;
//        float ScreenSizeX = prjT.m22   * 0.0002 * Kof ;
//        
//    //    self.sceneView.pointOfView.camera.usesOrthographicProjection = YES;
//   //     self.sceneView.pointOfView.camera.fieldOfView = 60;
//   //     self.sceneView.pointOfView.camera.zNear = 0.0005;
//   //     self.sceneView.pointOfView.camera.zFar  = 1000;
//   //     m_location.x = g_screenSize.x;
//   //     m_location.y = g_screenSize.y;
//   //     m_location.x = 0;
//   //     m_location.y = 0;
//        
//        
//        float CorrectY = (g_screenSize.x - m_location.x)/g_screenSize.x;
//        float CorrectX = (g_screenSize.y - m_location.y)/g_screenSize.y;
//    
//        CorrectY  = CorrectY * ScreenSizeX ;
//        CorrectX  = CorrectX * ScreenSizeY ;
//        
//     //   NSLog(@"Near %f Far %f",self.sceneView.pointOfView.camera.zNear,self.sceneView.pointOfView.camera.zFar);
//        
//        float m22 = -prjT.m33;
//        float m32 = -prjT.m43;
//        
//        float near = (2.0f*m32)/(2.0f*m22-2.0f);
//        float far = ((m22-1.0f)*near)/(m22+1.0);
//        
//        NSLog(@"near:%f   far:%f ",near,far);
//        //
//        
//        NSLog(@"CorX %f %f",prjT.m11,prjT.m22);
//        
//        [circle UpdateTransform:SCNVector3Make(CorrectX*-1, CorrectY*-1 , -far*4) : frame.camera : vec];
//   
//        
//        
//        
//        if (_aim_test != NULL){
//            CGPoint newpoint;
//            newpoint.x = (g_screenSize.x - m_location.x) * -1 + g_screenSize.x;
//            newpoint.y = (g_screenSize.y - m_location.y) * -1 + g_screenSize.y;
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//               
//                {
//                    self->_aim_test.center = newpoint;
//                }
//            });
//            
//        }
//       
//       
//
//
// 
////        NSLog(@"Correct %f %f",(g_screenSize.x - m_location.x)/g_screenSize.x,(g_screenSize.y - m_location.y)/g_screenSize.y);
//        
//       // [frame.camera projectPoint:location orientation:UIInterfaceOrientationUnknown viewportSize:self.view.frame.size];$
//        
//
////        SCNMatrix4 cameraPos = SCNMatrix4FromMat4(frame.camera.transform);
////
////        float f = fabsf(cameraPos.m11 - cameraPosLast.m11) +
////        fabsf(cameraPos.m12 - cameraPosLast.m12) +
////        fabsf(cameraPos.m13 - cameraPosLast.m13) +
////        fabsf(cameraPos.m14 - cameraPosLast.m14) +
////
////        fabsf(cameraPos.m21 - cameraPosLast.m21) +
////        fabsf(cameraPos.m22 - cameraPosLast.m22) +
////        fabsf(cameraPos.m23 - cameraPosLast.m23) +
////        fabsf(cameraPos.m24 - cameraPosLast.m24) +
////
////        fabsf(cameraPos.m31 - cameraPosLast.m31) +
////        fabsf(cameraPos.m32 - cameraPosLast.m32) +
////        fabsf(cameraPos.m33 - cameraPosLast.m33) +
////        fabsf(cameraPos.m34 - cameraPosLast.m34) +
////
////        fabsf(cameraPos.m41 - cameraPosLast.m41) +
////        fabsf(cameraPos.m42 - cameraPosLast.m42) +
////        fabsf(cameraPos.m43 - cameraPosLast.m43) +
////        fabsf(cameraPos.m44 - cameraPosLast.m44);
////        if ( f> 0.2)
////        {
////            NSLog(@"Last mat %@",[MathHelpers NSStringFromSTransform3D:cameraPosLast]);
////            NSLog(@"Cur mat %@",[MathHelpers NSStringFromSTransform3D:cameraPos]);
////            NSLog(@"Dif %f",f);
////        }
////
////
////        cameraPosLast = cameraPos;
//    }
}



- (void)session:(ARSession *)session cameraDidChangeTrackingState:(ARCamera *)camera
{
    g_state = camera.trackingState;
    int code=0;
    NSString *str;
   
    switch (g_state) {
        case ARTrackingStateNotAvailable:
            [self ChangeCircleColor:[UIColor redColor]];
            [self AddPulse];
            str = @"Tracking not available";
            code = -1;
            break;
        case ARTrackingStateNormal:
            [self ChangeCircleColor:[UIColor redColor]];
            [self AddPulse];
            str = @"Tracking normal";
            code = 0;
            break;
        case ARTrackingStateLimited:
            
            switch(camera.trackingStateReason)
        {
            case ARTrackingStateReasonInitializing:
                [self ChangeCircleColor:[UIColor redColor]];
                [self AddPulse];
                str = @"Tracking limited: Initializing";
                code = 1;
                break;
            case ARTrackingStateReasonRelocalizing:
                [self ChangeCircleColor:[UIColor redColor]];
                [self AddPulse];
                str = @"Tracking limited: Relocalizing";
                code = 2;
                break;
            case ARTrackingStateReasonExcessiveMotion:
                [self ChangeCircleColor:[UIColor redColor]];
                [self AddPulse];
                str = @"Tracking limited: Excessive Motion";
                code = 3;
                break;
            case ARTrackingStateReasonInsufficientFeatures:
                [self ChangeCircleColor:[UIColor redColor]];
                [self AddPulse];
                str = @"Tracking limited: Insufficient Features";
                code = 4;
                break;
            default:
                [self ChangeCircleColor:[UIColor redColor]];
                [self AddPulse];
                str = @"Tracking limited";
                code = 5;
                break;
        }
            break;
        default:
            break;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(trackingState:::)]) {
        [self.delegate trackingState:str:g_state:code];
    }
    
}

- (void)session:(ARSession *)session didFailWithError:(NSError *)error {
    // Present an error message to the user
    NSLog(@"Error ARSession: %@",error);
}

- (void)sessionWasInterrupted:(ARSession *)session {
    // Inform the user that the session has been interrupted, for example, by presenting an overlay
}

- (void)sessionInterruptionEnded:(ARSession *)session {
    // Reset tracking and/or remove existing anchors if consistent tracking is required
}



- (void)renderer:(id <SCNSceneRenderer>)renderer didAddNode:(SCNNode *)node forAnchor:(ARAnchor *)anchor
{
//    if ([anchor isKindOfClass:[ARPlaneAnchor class]]) {
//        NSLog(@"%@",[anchor identifier]);
//        NSLog(@"Find %@",[MathHelpers NSStringFromMTransform3D:anchor.transform]);
//        return;
//    }
}

- (void)renderer:(id <SCNSceneRenderer>)renderer didUpdateNode:(SCNNode *)node forAnchor:(ARAnchor *)anchor
{
//    if ([anchor isKindOfClass:[ARPlaneAnchor class]]) {
//        NSLog(@"%@",[anchor identifier]);
//        NSLog(@"Update %@",[MathHelpers NSStringFromMTransform3D:anchor.transform]);
//        return;
//    }
}

-(void)renderer:(id<SCNSceneRenderer>)renderer didRemoveNode:(SCNNode *)node forAnchor:(ARAnchor *)anchor
{
}


-(void) beginDrawGrid
{
//    NSLog(@"Begin tap");
    pressTap = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(beginDraw)]) {
        [self.delegate beginDraw];
    }

}

-(void) endDrawGrid
{
//    NSLog(@"End tap");
    pressTap = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(endDraw)]) {
        [self.delegate endDraw];
    }


}

-(void) GeneratePointAndDelegate
{
    [self GeneratePoint];
}

-(void) ChangeCircleColor:(UIColor*)_color
{
    [_delegateCircle circleLogicChangeColor:_color];
  
}

-(void) AddPulse
{
    [_delegateCircle circleLogicAddPulse];
}

-(void) RemovePulse
{
    [_delegateCircle circleLogicRemovePulse];
}

-(void) AddWaitCircle
{
    [_delegateCircle circleLogicSetWaitState];
}

-(void) RemoveWaitCircle
{
    [_delegateCircle circleLogicSetNormalState];
}

// Always return YES value if plane exists on point
-(BOOL) GeneratePoint
{
    ARFrame *frame = self.sceneView.session.currentFrame;
    if (!frame)
        return NO;
   
    
    SCNVector3 cameraPos = [MathHelpers positionFromTransform:frame.camera.transform];
    
    
    /// Calculate angle and offset
    float tilt = fabsf(frame.camera.eulerAngles[0]);
    float threshold1 = M_PI / 2 * 0.65;
    float threshold2 = M_PI / 2 * 0.75;
    float yaw = atan2f(frame.camera.transform.columns[0].x,frame.camera.transform.columns[1].x);
    float angle = 0;
    int   useXAngleRotate = YES;
    
    if (0 <= tilt && tilt <threshold1)
    {
        angle = frame.camera.eulerAngles[1];
        useXAngleRotate = NO;
    }
    else
        if (threshold1 <= tilt && tilt <threshold2)
        {
            float relativeInRange = fabsf((tilt - threshold1) / (threshold2 - threshold1));
            
            float normalized = frame.camera.eulerAngles.y;
            while (fabsf(normalized - yaw) > M_PI / 4) {
                if (frame.camera.eulerAngles.y > yaw) {
                    normalized -= M_PI / 2;
                } else {
                    normalized += M_PI / 2;
                }
            }
            
            
            //  = normalize(camera.eulerAngles.y, forMinimalRotationTo: yaw);
            angle = normalized * (1 - relativeInRange) + yaw * relativeInRange;
        }
        else
        {
            angle = yaw;
        }
    
   // NSLog(@"Angles %f", angle);
    
    
    if (g_state!= ARTrackingStateNormal)
        return NO;
    

    
  
    //CGPoint location2 = self.view.center;
  

    ARAnchor* anchor = [m_plans GetBaseAnchor];

    SCNVector3 position = SCNVector3Make(0, 0, 0);
    ARWorldCoordCalc resultcalc = ARWorldCoordCalcNone;
    BOOL findFlag = NO;
    
    if (anchor)
    {
        ARPlaneAnchor* planeanchor = (ARPlaneAnchor*)anchor;
        SCNVector3 anchorposition = SCNVector3Make(planeanchor.transform.columns[3].x, planeanchor.transform.columns[3].y, planeanchor.transform.columns[3].z);
        findFlag = [MathHelpers HitTestByHorizontalPlaneFromCamera:self.sceneView :m_location:anchorposition:&position];
        // GetFocused Anchor;
    }
    else
    {
        resultcalc = [MathHelpers worldPositionFromScreenPosition:self.sceneView: m_location: SCNVector3Make(0, 0, 0) :YES :&anchor :&position];
    }
   
    
    if (resultcalc != ARWorldCoordCalcNone || findFlag)
    {
        //   NSLog(@"Recogonize 3d point %d",resultcalc);
 //          NSLog(@"Position ( %f , %f , %f)",position.x,position.y,position.z);
//        if (![anchor isEqual:G_anchor] && anchor.transform.columns[3].w != 0)
//        {
//            NSLog(@"Anchor =%@",[MathHelpers NSStringFromMTransform3D:anchor.transform]);
//            G_anchor =anchor;
//        }
//        boxNode.position = position;
     
            
        
        if ([anchor isKindOfClass:[ARPlaneAnchor class]] )
        {
            
          
            
            if ([m_plans PointToPlane:position])
            {
                if (self.delegate && [self.delegate respondsToSelector:@selector(planIsFound:)])
                    [self.delegate planIsFound:YES];
                
                if (!g_connected)
                {
                    [self ChangeCircleColor:[UIColor redColor]];
                    [self AddPulse];
                    return NO;
                }
                
                if (!g_balanceState)
                {
                    [self ChangeCircleColor:[UIColor redColor]];
                    [self RemovePulse];
                    return NO;
                }
                
                if (!pressTap)
                {
                    [self ChangeCircleColor:[UIColor orangeColor]];
                    [self RemovePulse];
                    [self RemoveWaitCircle];
                    
                    if (self.delegate) [self.delegate showMessage:@"Press multi-functional button to start scanning" :[UIColor orangeColor]];
                        
                    return YES;
                }
                
               
                float len = [MathHelpers Lenght:[MathHelpers Sub:position :cameraPos]];
                if ((len > 1.4f) || g_gyroOutOfRange)
                {
                    [self ChangeCircleColor:[UIColor redColor]];
                    
                    if (self.delegate) [self.delegate showMessage:@"Improper position of the device." :[UIColor redColor]];
                    
                    return YES;
                }
                else
                    if (self.delegate) [self.delegate showMessage:@"Scan in progress" :[UIColor greenColor]];
                
                [self ChangeCircleColor:[UIColor greenColor]];
                [self RemovePulse];
                [self RemoveWaitCircle];
                 @synchronized(m_plans) {
                     
                    if (![m_plans IsContain:position])
                    {
                        
                    //    if ([m_plans GetCount] == 0)
                     //   {
                     //       [self.sceneView.session setWorldOrigin:frame.camera.transform];
                    //    }
                        
                        [m_plans CorrectAnchor:(ARPlaneAnchor*)anchor];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(drawGridNodeWithValue)])
                        {
                            NSNumber* value = [self.delegate drawGridNodeWithValue];
                            ARPlaneGridItem *plane = [[ARPlaneGridItem alloc] initWithVector:position:m_sizeCell.height:m_sizeCell.width];
                            
                            
                            if ([m_plans AddItem:plane :angle])
                            {
                                ARPlaneGridItem *grid_plane = [m_plans GenerateGridCellPlane:plane];
                                [UIARTextureHelper ConfiguratePlane:m_plans:plane:value.floatValue:g_texturealpha];
                                [self.sceneView.scene.rootNode addChildNode:plane];
                                [self.sceneView.scene.rootNode addChildNode:grid_plane];
                                
                            }
                        }
                        
                        
                        
    //                  NSLog(@"Gen plane");
                    }
                    else
                    {
                        if (self.delegate && [self.delegate respondsToSelector:@selector(drawGridNodeWithValue)])
                        {
                            NSNumber* value = [self.delegate drawGridNodeWithValue];
                            SCNVector3 index= [m_plans GenIndex:position];
                            if (index.y == 1)
                            {
                                ARPlaneGridItem *plane = [m_plans GetPlaneAtIndex:index];                        
                                [UIARTextureHelper ConfiguratePlane:m_plans:plane:value.floatValue:g_texturealpha];
                            }
                        }
                    }
                }
                return YES;
            }
            if (self.delegate && [self.delegate respondsToSelector:@selector(planIsFound:)])
                [self.delegate planIsFound:NO];
        }
        else
        {
            [self ChangeCircleColor:[UIColor redColor]];
            [self AddPulse];
            [self AddWaitCircle];
            
        }
        
    }
    
    
    return NO;
}



-(void) setSizeCell:(CGSize)_sizeCell
{
    m_sizeCell = _sizeCell;
    [m_plans ClearItems];
}

-(void) clearGrid
{
    [m_plans ClearItems];
}

-(void) resetAR
{
    if (m_isResetAR)
        return;
    m_isResetAR = YES;
    [self.sceneView.session pause];
    [m_plans ClearItems];
    m_plans = [[ARPlaneGrid alloc] init];
    [m_plans SetDrawModeForAllPlane:m_drawGrid];
    ARWorldTrackingConfiguration *configuration = [[ARWorldTrackingConfiguration alloc] init];
    configuration.planeDetection = ARPlaneDetectionHorizontal ;
    configuration.lightEstimationEnabled  = YES;
    [self.sceneView.session runWithConfiguration:configuration options:ARSessionRunOptionRemoveExistingAnchors| ARSessionRunOptionResetTracking ];
    m_isResetAR = NO;
}

-(void) setDrawGridMode:(BOOL)_drawGrid
{
    @synchronized(self)
    {
        m_drawGrid = _drawGrid;
        [m_plans SetDrawModeForAllPlane:_drawGrid];
    }
}

-(float*) generateDataForCSV:(int*) _width :(int*) _height
{
    return [self generateDataForCSV:_width:_height:0];
}

-(float*) generateDataForCSV:(int*) _width :(int*) _height :(float) _defaultValue
{
    int yMin = INT_MAX, xMin = INT_MAX;
    int yMax = INT_MIN, xMax = INT_MIN;
    
    for (ARPlaneGridItem *tempObject in [m_plans getEnumerator])
    {
        if (tempObject.indexPlane.x < xMin)
            xMin = tempObject.indexPlane.x;
        if (tempObject.indexPlane.x > xMax)
            xMax = tempObject.indexPlane.x;
        
        if (tempObject.indexPlane.z < yMin)
            yMin = tempObject.indexPlane.z;
        if (tempObject.indexPlane.z > yMax)
            yMax = tempObject.indexPlane.z;
    }
    int width = xMax - xMin+1;
    int height = yMax - yMin+1;
    float *tmpMapArray = (float *)malloc(sizeof(float) * width * height);
    
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
            tmpMapArray[i * width + j] = _defaultValue;
    }
   
    for (ARPlaneGridItem *tempObject in [m_plans getEnumerator])
    {
       tmpMapArray[((int)tempObject.indexPlane.z - yMin) * width + ((int)tempObject.indexPlane.x - xMin)] = tempObject.value;
    }
    *_width = width;
    *_height = height;
    return tmpMapArray;
}

-(void) SetBalanceState:(BOOL)_state
{
    g_balanceState = _state;
}

-(void) SetConnectedState:(BOOL)_state
{
    g_connected = _state;
}







-(float) getTextureAlpha
{
    return g_texturealpha;
}

-(void)  setTextureAlpha:(float) _value
{
    g_texturealpha  = _value;
    for (ARPlaneGridItem *tempObject in [m_plans getEnumerator])
    {
        tempObject.geometry.firstMaterial.transparency = _value;
    }
}

-(void) setGyroOutOfRange:(BOOL)_value
{
    g_gyroOutOfRange = _value;
}

-(void) moveCircle:(int) state :(CGPoint)_position{
    m_location.x = _position.x;
    m_location.y = _position.y;
    
    switch (state) {
        case 1:
            m_animationMove = YES;
            break;

        case 3: 
            m_animationMove = NO;
            break;
        default:
            break;
    }
}

@end
