

#import "ARFloor.h"

@implementation ARFloor

-(id) initWithAnchor:(ARPlaneAnchor*)anchor
{
    if ( self = [super init] ) {
  
        self.geometry = [SCNFloor new];
        self.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z);
    }
    return self;
}

@end
