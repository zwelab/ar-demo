

#import <CoreMotion/CoreMotion.h>
#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

#import "UIARkitExtends.h"

API_AVAILABLE(ios(11.0))
@interface CircleNode2 : SCNNode <SCNSceneRendererDelegate>{
    NSMutableArray *m_lastpositions;
    UIColor *m_current_color;
    UIColor *m_save_color;
    bool m_lockcolor;
    SCNScene *m_scene;
    CGSize m_screenSize;
    CGSize m_frameSizeinPix;
    SCNNode* m_cameranode;
    bool m_animationMove;
    CGPoint m_location;
    CMMotionManager* motionManager;
    
}


@property (nonatomic, weak) UIImageView *aim_test;

-(id) init;
-(void) setScreenSize:(float) width :(float) height;
-(bool) moveCircle:(int) state :(CGPoint)_position;
-(void) startMotionManager;
-(void) UpdateTransform;
-(void) saveLocation;

-(void) SetNormalState;
-(void) SetWaitState;
-(void) ChangeColor:(UIColor*) _color;
-(UIColor*) getColor;
-(void) AddPulse;
-(void) RemovePulse;
-(void) LockColor;
-(void) UnlockColor;
-(SCNScene*) getScane;
-(SCNNode*)  getCamera;

@end
