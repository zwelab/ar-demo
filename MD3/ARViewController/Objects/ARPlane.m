

#import "ARPlane.h"

@implementation ARPlane

-(id) initWithAnchor:(ARPlaneAnchor*)anchor
{
    CGFloat width = (CGFloat)anchor.extent.x;
    CGFloat height = (CGFloat)anchor.extent.z;
    
    if ( self = [super init] ) {
        
    
        self.geometry = [SCNPlane planeWithWidth:width height:height];
        CGFloat x = anchor.transform.columns[3].x;
        CGFloat y = anchor.transform.columns[3].y;
        CGFloat z = anchor.transform.columns[3].z;
        self.position = SCNVector3Make(0, 0, 0);
        self.transform = SCNMatrix4MakeTranslation(x,y,z);
        self.transform = SCNMatrix4Mult( SCNMatrix4MakeRotation(-M_PI / 2, 1, 0, 0),self.transform);
            
    }
    return self;
}

-(id) initWithVector:(SCNVector3)_vector :(CGFloat)_sizeX :(CGFloat)_sizeY
{
    
    if ( self = [super init] ) {
        SizeX = _sizeX;
        SizeY = _sizeY;
        
        self.geometry = [SCNPlane planeWithWidth:SizeX height:SizeY];
        CGFloat x = _vector.x;
        CGFloat y = _vector.y;
        CGFloat z = _vector.z;
        self.position = SCNVector3Make(0, 0, 0);
        self.transform = SCNMatrix4MakeTranslation(x,y,z);
        self.transform = SCNMatrix4Mult( SCNMatrix4MakeRotation(-M_PI / 2, 1, 0, 0),self.transform);
        
    }
    return self;
}

-(void) setNewPosition:(SCNVector3)_vector
{
    CGFloat x = _vector.x;
    CGFloat y = _vector.y;
    CGFloat z = _vector.z;
    self.transform = SCNMatrix4MakeTranslation(x,y,z);
    self.transform = SCNMatrix4Mult( SCNMatrix4MakeRotation(-M_PI / 2, 1, 0, 0),self.transform);
}

-(void) setMoveToNewPosition:(SCNVector3)_vector
{
    CGFloat x = _vector.x;
    CGFloat y = _vector.y;
    CGFloat z = _vector.z;
    self.transform = SCNMatrix4Mult(SCNMatrix4MakeTranslation(x, y, z), self.transform);
   // self.transform = SCNMatrix4Mult( SCNMatrix4MakeRotation(-M_PI / 2, 1, 0, 0),self.transform);
}
@end
