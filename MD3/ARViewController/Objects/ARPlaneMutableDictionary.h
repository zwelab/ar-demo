

#import <Foundation/Foundation.h>

#import "ARPlaneGridItem.h"

API_AVAILABLE(ios(11.0))
@interface ARPlaneMutableDictionary:NSObject{
    
    NSMutableDictionary<NSNumber*,ARPlaneGridItem*> *m_planes;
    
}
-(id) init;
-(void) dealloc;

-(void) AddPlane:(NSNumber*) _id :(ARPlaneGridItem*) _plane_node;
-(void) RemovePlane:(NSNumber*) _id;
-(void) RemoveAllPlanes;
-(ARPlaneGridItem*) GetPlane:(NSNumber*) _id;
-(bool) UpdatePlanes:(NSNumber*)_id :(ARPlaneAnchor*)_anchor_plane;
-(NSUInteger)  Count;
-(NSEnumerator<ARPlaneGridItem*>*) getEnumerator;
@end
