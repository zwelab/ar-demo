

#import "CircleNode.h"

@implementation CircleNode


-(id) init
{
    if ( self = [super init] ) {
        
        SCNScene *scene = [SCNScene sceneNamed:@"Models.scnassets/Torus2.scn"];
        for(SCNNode* node in scene.rootNode.childNodes)
        {
           // if ([node.name compare:@"Torus"]==0 )
            {
                    [node removeFromParentNode];
                    [self addChildNode:node];
                    node.geometry.firstMaterial.diffuse.contents  =[UIColor colorWithRed:0
                                                                               green:0
                                                                                blue:0
                                                                               alpha:0];
                
            
            }
            m_lockcolor = NO;
        }
        m_lastpositions = [NSMutableArray new];
 
    }
    return self;
}

-(SCNMatrix4) CalcAVGPosition
{
    SCNMatrix4 m = SCNMatrix4Identity;
    m.m11 = m.m22 = m.m33 = m.m44 =  0;
    float size = m_lastpositions.count;
    for( NSValue *value in m_lastpositions)
    {
        SCNMatrix4 structValue;
        [value getValue:&structValue];
        m.m11 += structValue.m11;
        m.m12 += structValue.m12;
        m.m13 += structValue.m13;
        m.m14 += structValue.m14;
   
        m.m21 += structValue.m21;
        m.m22 += structValue.m22;
        m.m23 += structValue.m23;
        m.m24 += structValue.m24;
        
        m.m31 += structValue.m31;
        m.m32 += structValue.m32;
        m.m33 += structValue.m33;
        m.m34 += structValue.m34;
        
        m.m41 += structValue.m41;
        m.m42 += structValue.m42;
        m.m43 += structValue.m43;
        m.m44 += structValue.m44;
    }
    
    m.m11 /= size;
    m.m12 /= size;
    m.m13 /= size;
    m.m14 /= size;
    
    m.m21 /= size;
    m.m22 /= size;
    m.m23 /= size;
    m.m24 /= size;
    
    m.m31 /= size;
    m.m32 /= size;
    m.m33 /= size;
    m.m34 /= size;
    
  //  m.m41 /= size;
  //  m.m42 /= size;
  //  m.m43 /= size;
  //  m.m44 /= size;
   
    SCNMatrix4 structValue;
    SCNMatrix4 structValuePrev;
    [m_lastpositions.lastObject  getValue:&structValue];
    
    if (m_lastpositions.count > 2)
    {
        [[m_lastpositions objectAtIndex:m_lastpositions.count-2]  getValue:&structValuePrev];
        if (fabsf(structValue.m41 - structValuePrev.m41) > 0.1)
            NSLog(@"Circle debug %f",fabsf(structValue.m41 - structValuePrev.m41));
        if ((fabsf(structValue.m41 - structValuePrev.m41) > 1) ||
            (fabsf(structValue.m42 - structValuePrev.m42) > 1) ||
            (fabsf(structValue.m43 - structValuePrev.m43) > 1))
        {
            m.m41 = structValuePrev.m41;
            m.m42 = structValuePrev.m42;
            m.m43 = structValuePrev.m43;
            m.m44 = structValuePrev.m44;
        //    [m_lastpositions removeLastObject];
        }
        else
        {
            m.m41 = structValue.m41;
            m.m42 = structValue.m42;
            m.m43 = structValue.m43;
            m.m44 = structValue.m44;
        }
    }
    

    
    return m;
}

-(void) UpdateTransform:(SCNVector3)_postition :(ARCamera*)_camera :(SCNVector3) correct
{
    
    SCNMatrix4 cameraPos = SCNMatrix4FromMat4(_camera.transform);
    SCNMatrix4 moveMatrix = SCNMatrix4MakeTranslation(_postition.x, _postition.y, _postition.z);
    //   SCNMatrix4 correctMatrix = SCNMatrix4MakeTranslation(_postition.x, _postition.y, _postition.z);
    moveMatrix = SCNMatrix4Mult( moveMatrix,cameraPos);
    // moveMatrix = SCNMatrix4Mult( moveMatrix, correctMatrix);
    moveMatrix = SCNMatrix4Mult( SCNMatrix4MakeScale(0.00005, 0.00005, 0.00005) ,moveMatrix);
    
    NSValue *value = [NSValue valueWithBytes:&moveMatrix objCType:@encode(SCNMatrix4)];
    [m_lastpositions addObject: value];
    if ([m_lastpositions count] > 40)
        [m_lastpositions removeObjectAtIndex:0];
    
    self.transform = [self CalcAVGPosition];

}


-(void) SetNormalState
{
   

    for(SCNNode* node in self.childNodes)
    {
        if ([node.name compare:@"Torus"]==0)
        {
            [SCNTransaction begin];
            [SCNTransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            [SCNTransaction setAnimationDuration:1];
            node.geometry.firstMaterial.multiply.contents = [UIImage imageNamed:@"CircleNormal"];

            [SCNTransaction commit];
            
            [node removeActionForKey:@"rotate"];
        }
    }
}

-(void) SetWaitState
{
    for(SCNNode* node in self.childNodes)
    {
        if ([node.name compare:@"Torus"]==0)
        {
            
            [SCNTransaction begin];
            [SCNTransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            [SCNTransaction setAnimationDuration:1];
            
            node.geometry.firstMaterial.multiply.contents = [UIImage imageNamed:@"CircleWait"];
            
            SCNAction *action = [SCNAction rotateByX:0 y:0 z:-2 duration:1];
            [SCNTransaction commit];
            
            [node runAction:[SCNAction repeatActionForever:action] forKey:@"rotate"];
        }
    }
    
}

-(void) AddPulse
{
    for(SCNNode* node in self.childNodes)
    {
        if ([node.name compare:@"Torus"]==0)
        {
            if (![node actionForKey:@"pulse"])
            {
                
                SCNAction* pulseOutAction = [SCNAction scaleBy:10.0f/9.0f duration:0.4];
                SCNAction* pulseInAction = [SCNAction scaleBy:9.0f/10.0f duration:0.4];
                pulseOutAction.timingMode = SCNActionTimingModeEaseOut;
                pulseInAction.timingMode = SCNActionTimingModeEaseOut;
                
        
                [node runAction:[SCNAction repeatActionForever:[SCNAction sequence:@[pulseOutAction,pulseInAction]]] forKey:@"pulse"];

            }
            else
            {
                SCNAction *act =  [node actionForKey:@"pulse"];
                [act setSpeed:0.8];
            }
        }
    }
}

-(void) RemovePulse
{
    for(SCNNode* node in self.childNodes)
    {
        if ([node.name compare:@"Torus"]==0)
        {
            SCNAction *act =  [node actionForKey:@"pulse"];
            [act setSpeed:0];
        }
    }
}

-(void) ChangeColor:(UIColor*) _color
{
    
    if (m_lockcolor)
        return;

    m_current_color = _color;
    for(SCNNode* node in self.childNodes)
    {
        if ([node.name compare:@"Torus"]==0)
        {
            [SCNTransaction begin];
            [SCNTransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            [SCNTransaction setAnimationDuration:0.8];
            node.geometry.firstMaterial.diffuse.contents  = _color;
            [SCNTransaction commit];
        }
    }
    
}

-(UIColor*) getColor{
    return m_current_color;
}

-(void) LockColor{
    m_lockcolor = YES;
}

-(void) UnlockColor{
    m_lockcolor = NO;
}

@end
