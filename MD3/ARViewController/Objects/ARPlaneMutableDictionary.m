

#import "ARPlaneMutableDictionary.h"

@implementation ARPlaneMutableDictionary

-(id) init
{
    if (self = [super init])
    {
        m_planes = [[NSMutableDictionary<NSNumber*,ARPlaneGridItem*> alloc] init];
    }
    return self;
}

-(void) dealloc
{
    [self RemoveAllPlanes];
}

-(void) AddPlane:(NSNumber*) _id :(ARPlaneGridItem*) _plane_node
{
    [m_planes setObject:_plane_node forKey:_id];
}

-(void) RemovePlane:(NSNumber*) _id
{
    if(!m_planes[_id])
        [m_planes[_id] removeFromParentNode];
    [m_planes removeObjectForKey:_id];
}

-(void) RemoveAllPlanes
{
    NSEnumerator *enumerator = [m_planes objectEnumerator] ;
    ARPlaneGridItem* anObject;
    while (anObject = (ARPlaneGridItem*)[enumerator nextObject]) {
        [anObject removeFromParentNode];
    }
    [m_planes removeAllObjects];
}

-(ARPlane*) GetPlane:(NSNumber*) _id
{
    return m_planes[_id];
}



-(bool) UpdatePlanes:(NSNumber*)_id :(ARPlaneAnchor*)_anchor_plane
{
    ARPlaneGridItem* plane = m_planes[_id];
    if (!plane)
        return NO;
    
    plane.position = SCNVector3Make(_anchor_plane.center.x, 0, _anchor_plane.center.z);
    ((SCNPlane*)plane.geometry).width= _anchor_plane.extent.x;
    ((SCNPlane*)plane.geometry).height= _anchor_plane.extent.z;
    
    return YES;
}

-(NSUInteger)  Count
{
    return [m_planes count];
}

-(NSEnumerator<ARPlaneGridItem*>*) getEnumerator
{
    return [m_planes objectEnumerator];
}

@end
