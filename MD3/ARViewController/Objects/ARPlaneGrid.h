

#import <Foundation/Foundation.h>

#import "ARPlaneGridItem.h"
#import "ARPlaneMutableDictionary.h"


API_AVAILABLE(ios(11.0))
@interface ARPlaneGrid : NSObject
{
    ARPlaneMutableDictionary* m_grid;
    ARPlaneMutableDictionary* m_grid_with_rect;
    ARPlaneAnchor* m_base_anchor;
    SCNVector3 m_correct_by_new_anchor;
    CGSize m_griditemsize;
    SCNVector3 m_startPosition;
    BOOL m_gridVisible;
    float m_aligment_angle;
    SCNMatrix4 m_newlocalcoord;
}
//@property (nonatomic,weak) SCNNode *boxNode;
//@property (nonatomic,weak) SCNNode *boxNode2;

-(id) init;
-(bool) AddItem:(ARPlaneGridItem*)_plane :(float) _angle;
-(ARPlaneGridItem*) GenerateGridCellPlane:(ARPlaneGridItem*)_plane;
-(SCNVector3) GenIndex:(SCNVector3)_position;

-(bool) IsContain:(SCNVector3)_position;
-(bool) PointToPlane:(SCNVector3)_position;
-(ARPlaneGridItem*) GetPlaneAtIndex:(SCNVector3)_index;
-(void) ClearItems;
-(void) SetDrawModeForAllPlane:(BOOL)_drawGrid;
-(NSEnumerator<ARPlaneGridItem*>*) getEnumerator;
-(int) GetCount;
-(void) CorrectAnchor:(ARPlaneAnchor*)_anchor;
-(ARPlaneAnchor*) GetBaseAnchor;


@end
