

#import "ARPlane.h"
API_AVAILABLE(ios(11.0))
@interface ARPlaneGridItem : ARPlane
{
@public
    float m_texture_val[3][3];
}

@property (nonatomic) SCNVector3 indexPlane;
@property (nonatomic) float value;


-(id) initWithVector:(SCNVector3)_position;
-(id) initWithVector:(SCNVector3)_position :(CGFloat)_sizeX :(CGFloat)_sizeY;
-(CGSize) GetPlaneSize;


@end
