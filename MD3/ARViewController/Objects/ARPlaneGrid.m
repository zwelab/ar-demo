
#import "ARPlaneGrid.h"
#import "UIARTextureHelper.h"
#import "../UIARkitExtends.h"

@implementation ARPlaneGrid

static NSInteger st_g_planeIndex = 0;

-(id) init
{
    if (self = [super init])
    {
        m_grid = [[ARPlaneMutableDictionary alloc] init];
        m_grid_with_rect = [[ARPlaneMutableDictionary alloc] init];
        m_aligment_angle = 0;
    }
    return self;
};

-(bool) AddItem:(ARPlaneGridItem*)_plane :(float) _angle
{
    if ([m_grid Count] ==0)
    {
        m_aligment_angle = _angle;
        
        m_griditemsize = [_plane GetPlaneSize];
        //NSLog(@"%@",  [MathHelpers NSStringFromSTransform3D:_plane.transform] );
        m_startPosition = SCNVector3Make(_plane.transform.m41,_plane.transform.m42,_plane.transform.m43);
        m_newlocalcoord = SCNMatrix4MakeTranslation(_plane.transform.m41,_plane.transform.m42,_plane.transform.m43);
    //    _plane.transform = SCNMatrix4Rotate(_plane.transform, m_aligment_angle, 0, 1, 0);
    //    _plane.rotation = SCNVector4Make(0, 1, 0, angle);
     //   NSLog(@"%@",[MathHelpers NSStringFromSTransform3D:_plane.transform]);
     //   NSLog(@"%@",[MathHelpers NSStringFromSTransform3D:SCNMatrix4Rotate(_plane.transform, m_aligment_angle, 0, 1, 0)]);
        m_newlocalcoord = SCNMatrix4Mult(SCNMatrix4MakeRotation(m_aligment_angle, 0, 1,0),m_newlocalcoord);
        SCNMatrix4  m_newcoord = SCNMatrix4Mult(SCNMatrix4MakeRotation(-M_PI_2, 1, 0,0),m_newlocalcoord);
        
        _plane.transform = m_newcoord;
    //    NSLog(@"%@",[MathHelpers NSStringFromSTransform3D:_plane.transform]);
        
        _plane.indexPlane = SCNVector3Make(0, 1, 0);
        [m_grid AddPlane:[NSNumber numberWithInteger:st_g_planeIndex++] :_plane];
        
    //   NSLog(@"Degree %f",m_aligment_angle * 180 / M_PI);
        
    }
    else
    {
        SCNVector3 Position = SCNVector3Make(_plane.transform.m41,_plane.transform.m42,_plane.transform.m43);
 
        SCNVector3 index = [self GenIndex:Position];
        if (index.y == 1)
        {
            //_plane setNewPosition:SCNVector3Make(  m_startPosition.x , m_startPosition.y,  m_startPosition.z)];
            _plane.transform = m_newlocalcoord;
            _plane.transform = SCNMatrix4Mult(SCNMatrix4MakeRotation(-M_PI_2, 1, 0,0),m_newlocalcoord);
            _plane.transform = SCNMatrix4Mult(SCNMatrix4MakeTranslation(index.x * m_griditemsize.width, -index.z * m_griditemsize.height,0  ),_plane.transform);
            _plane.indexPlane = index;
            [m_grid AddPlane:[NSNumber numberWithInteger:st_g_planeIndex++] :_plane];
     //       NSLog(@"Degree %f",m_aligment_angle * 180 / M_PI);
            
        }
        else
            return NO;
    }
    return YES;
};

-(ARPlaneGridItem*) GenerateGridCellPlane:(ARPlaneGridItem*)_plane
{
 //   SCNVector3 gridPosition =  SCNVector3Make(_plane.position.x,_plane.position.y+0.005,_plane.position.z);
   
 //   _plane.transform = SCNMatrix4Mult(SCNMatrix4MakeRotation(m_aligment_angle, 0, 0, 1),_plane.transform);
 //   _plane.transform = SCNMatrix4Mult(SCNMatrix4MakeTranslation(index.x * m_griditemsize.width, 0, index.z * m_griditemsize.height ),_plane.transform);
    SCNVector3 gridPosition =  SCNVector3Make(1,1,1);
    ARPlaneGridItem *plane = [[ARPlaneGridItem alloc] initWithVector:gridPosition:[_plane GetPlaneSize].width :[_plane GetPlaneSize].height];
    plane.transform = SCNMatrix4Mult(SCNMatrix4MakeRotation(-M_PI_2, 1, 0,0), plane.transform);
    plane.transform = SCNMatrix4Mult(_plane.transform,SCNMatrix4MakeTranslation( 0, 0.005, 0));
    plane.geometry.materials.firstObject.diffuse.contents = [UIImage imageNamed:@"GridCell"];
    [m_grid_with_rect AddPlane:[NSNumber numberWithInteger:st_g_planeIndex-1]  :plane];
    [plane setHidden:!m_gridVisible];
    return plane;
}

-(void) CorrectAnchor:(ARPlaneAnchor*)_anchor
{
    if (!m_base_anchor)
    {
        m_base_anchor = _anchor;
    }
//    else
//    {
//        if (_anchor.identifier == m_base_anchor.identifier)
//        {
//            m_correct_by_new_anchor = SCNVector3Make(m_base_anchor.transform.columns[3].x - _anchor.transform.columns[3].x,                                                m_base_anchor.transform.columns[3].y - _anchor.transform.columns[3].y,
//                                                     m_base_anchor.transform.columns[3].z - _anchor.transform.columns[3].z);
//            
//            for(ARPlaneGridItem * item in [m_grid getEnumerator])
//            {
//                
//                [item setMoveToNewPosition:m_correct_by_new_anchor];
//            }
//            m_startPosition.x = m_startPosition.x + m_correct_by_new_anchor.x;
//            m_startPosition.y = m_startPosition.y + m_correct_by_new_anchor.y;
//            m_startPosition.z = m_startPosition.z + m_correct_by_new_anchor.z;
//            m_base_anchor = _anchor;
//            NSLog(@"Correct Anchor (%f %f %f)",m_correct_by_new_anchor.x,m_correct_by_new_anchor.y,m_correct_by_new_anchor.z);
//        }
//    }
}

-(ARPlaneAnchor*) GetBaseAnchor
{
    return m_base_anchor;
}

-(SCNVector3) GenIndex:(SCNVector3)_position
{

//
//    SCNVector3 newCalcCoord =  SCNVector3Make(_position.x - m_startPosition.x, _position.y, _position.z - m_startPosition.z);
//    newCalcCoord = [MathHelpers MulVecMat:newCalcCoord :SCNMatrix4MakeRotation(m_aligment_angle, 0, 1,0)];
//   // newCalcCoord = SCNMatrix4Mult(newCalcCoord,SCNMatrix4MakeRotation(m_aligment_angle, 0, 1,0));
//    SCNVector3 coor = SCNVector3Make(newCalcCoord.x +  m_startPosition.x, newCalcCoord.y, newCalcCoord.z +  m_startPosition.z);
// //   CGFloat x_idx = (newCalcCoord.m41) / (m_griditemsize.width);
// //   CGFloat z_idx = (newCalcCoord.m43) / (m_griditemsize.height);
//    _boxNode.position = coor;
//    _boxNode2.position = _position;
//    NSLog(@"Degree %f",m_aligment_angle * 180 / M_PI);
//    NSLog(@"Pos %f %f %f",_position.x,_position.y,_position.z);
//    NSLog(@"NewPos %f %f %f",coor.x,coor.y,coor.z);
    
    _position =  SCNVector3Make(_position.x - m_startPosition.x, _position.y, _position.z - m_startPosition.z);
    _position = [MathHelpers MulVecMat:_position :SCNMatrix4MakeRotation(-m_aligment_angle, 0, 1,0)];
    // For understand logic back rotate
    _position =  SCNVector3Make(_position.x +  m_startPosition.x, _position.y, _position.z +  m_startPosition.z);
    
    CGFloat x_idx = (_position.x - m_startPosition.x) / (m_griditemsize.width);
    CGFloat z_idx = (_position.z - m_startPosition.z) / (m_griditemsize.height);
    //SCNVector3 indexRot = [MathHelpers MulVecMat:SCNVector3Make(x_idx, 0, z_idx) :SCNMatrix4MakeRotation(m_aligment_angle, 0, 1,0)];
   // x_idx = indexRot.x;
   // z_idx = indexRot.z;
    
   // NSLog(@"Index %f %f",x_idx,z_idx);
    int ix_idx =0;
    int iz_idx = 0;
    
    int xsig = (x_idx < 0 ? -1 : 1);
    int zsig = (z_idx < 0 ? -1 : 1);
    
    x_idx *= xsig;
    z_idx *= zsig;
    
    int FindFlagX = 0;
    int FindFlagZ = 0;
    
    if (fabs(x_idx - (int)x_idx) <0.35)
    {
        ix_idx = floorf(x_idx);
        FindFlagX = 1;
    }
    
    if (fabs(x_idx - (int)x_idx) >0.65)
    {
        ix_idx = ceilf(x_idx);
        FindFlagX = 1;
    }
    
    if (fabs(z_idx - (int)z_idx) <0.35)
    {
        iz_idx = floorf(z_idx);
        FindFlagZ = 1;
        
    }
    
    if (fabs(z_idx - (int)z_idx) >0.65)
    {
        iz_idx = ceilf(z_idx);
        FindFlagZ = 1;
    }
    
    ix_idx *= xsig;
    iz_idx *= zsig;
    
 //    NSLog(@"Round index %d %d",ix_idx,iz_idx);
    
    return SCNVector3Make(ix_idx, FindFlagX * FindFlagZ, iz_idx);
}

-(bool) IsContain:(SCNVector3)_position
{
    SCNVector3 index = [self GenIndex:_position];
    for (ARPlaneGridItem *tempObject in [m_grid getEnumerator]) {
        if (tempObject.indexPlane.x == index.x  && tempObject.indexPlane.z == index.z)
            return YES;
    }
    
    return NO;
}

-(ARPlaneGridItem*) GetPlaneAtIndex:(SCNVector3)_index
{
    for (ARPlaneGridItem *tempObject in [m_grid getEnumerator]) {
        if (tempObject.indexPlane.x == _index.x && tempObject.indexPlane.z == _index.z)
            return tempObject;
    }
    return nil;
}

-(bool) PointToPlane:(SCNVector3)_position
{
    if ([m_grid Count] == 0)
        return YES;
    
    if (fabsf(_position.y -m_startPosition.y) < 0.1)
        return YES;
    return NO;
}

-(void) ClearItems
{
    [m_grid RemoveAllPlanes];
    [m_grid_with_rect RemoveAllPlanes];

}

-(int) GetCount
{
    return (int)[m_grid Count];
}


-(void) SetDrawModeForAllPlane:(BOOL)_drawGrid
{
    m_gridVisible = _drawGrid;
    
    for(ARPlaneGridItem *item in [m_grid_with_rect getEnumerator])
    {
        [item setHidden:!m_gridVisible];
    }
}

-(NSEnumerator<ARPlaneGridItem*>*) getEnumerator
{
    return [m_grid getEnumerator];
}
@end
