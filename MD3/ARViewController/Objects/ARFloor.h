

#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

API_AVAILABLE(ios(11.0))
@interface ARFloor : SCNNode{
    
}


-(id) initWithAnchor:(ARPlaneAnchor*)anchor;
@end
