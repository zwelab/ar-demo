

#import "ARPlaneGridItem.h"

@implementation ARPlaneGridItem


-(id) initWithVector:(SCNVector3)_position
{
    if (self = [super initWithVector:_position :0.01 :0.01])
    {
        m_texture_val[0][0] = -1010;
        m_texture_val[0][1] = -1010;
        m_texture_val[0][2] = -1010;
        
        m_texture_val[1][0] = -1010;
        m_texture_val[1][1] = -1010;
        m_texture_val[1][2] = -1010;
        
        m_texture_val[2][0] = -1010;
        m_texture_val[2][1] = -1010;
        m_texture_val[2][2] = -1010;
    }
    return self;
}

-(id) initWithVector:(SCNVector3)_position :(CGFloat)_sizeX :(CGFloat)_sizeY
{
    if (self = [super initWithVector:_position :_sizeX :_sizeY])
    {
        m_texture_val[0][0] = -1010;
        m_texture_val[0][1] = -1010;
        m_texture_val[0][2] = -1010;
        
        m_texture_val[1][0] = -1010;
        m_texture_val[1][1] = -1010;
        m_texture_val[1][2] = -1010;
        
        m_texture_val[2][0] = -1010;
        m_texture_val[2][1] = -1010;
        m_texture_val[2][2] = -1010;
    }
    return self;
}

-(CGSize) GetPlaneSize
{
    return CGSizeMake(SizeX , SizeY);
}
@end
