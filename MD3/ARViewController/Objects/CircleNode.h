
#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

#import "UIARkitExtends.h"

API_AVAILABLE(ios(11.0))
@interface CircleNode : SCNNode{
    NSMutableArray *m_lastpositions;
    UIColor *m_current_color;
    bool m_lockcolor;
}

-(id) init;
-(void) UpdateTransform:(SCNVector3)_postition :(ARCamera*)_camera :(SCNVector3) correct;
-(void) SetNormalState;
-(void) SetWaitState;
-(void) ChangeColor:(UIColor*) _color;
-(UIColor*) getColor;
-(void) AddPulse;
-(void) RemovePulse;
-(void) LockColor;
-(void) UnlockColor;

@end
