
#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

API_AVAILABLE(ios(11.0))
@interface ARPlane : SCNNode{
@protected
    CGFloat SizeX;
    CGFloat SizeY;
}


-(id) initWithAnchor:(ARPlaneAnchor*)_anchor;

-(id) initWithVector:(SCNVector3)_vector :(CGFloat)_sizeX :(CGFloat)_sizeY;

-(void) setNewPosition:(SCNVector3)_vector;

-(void) setMoveToNewPosition:(SCNVector3)_vector;
@end
