

#import "CircleNode2.h"
#import "GroundScan3dSettingsViewController.h"

@implementation CircleNode2


-(id) init
{
    if ( self = [super init] ) {
        
        motionManager  = [[CMMotionManager alloc] init];
        m_scene = [SCNScene sceneNamed:@"Models.scnassets/Torus2.scn"];
    
        m_lockcolor = NO;
        m_lastpositions = [NSMutableArray new];
        
        SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Group" recursively:YES];
        node.position = SCNVector3Make(0, 0, 0);
        
        
        
        SCNNode *ambientLightNode = [SCNNode node];
        ambientLightNode.light = [SCNLight light];
        ambientLightNode.light.type = SCNLightTypeAmbient;
        ambientLightNode.light.color = [UIColor darkGrayColor];
        [m_scene.rootNode addChildNode:ambientLightNode];
        
        SCNCamera *camera = [SCNCamera camera];
        //  camera.zNear = 1;
        //  camera.zFar = 100;
        camera.usesOrthographicProjection = YES;
        camera.fieldOfView = 60;
        
        m_cameranode = [SCNNode node];
        m_cameranode.camera = camera;
        m_cameranode.position = SCNVector3Make(0, 0, 15);
        [m_scene.rootNode addChildNode:m_cameranode];
        
    }
    return self;
}

-(void) startMotionManager{
    motionManager.gyroUpdateInterval = 0.03;
    
    [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:
     ^(CMDeviceMotion* motion, NSError* error) {
         [self gyroUpdate: motion.attitude.roll: motion.attitude.pitch: motion.attitude.yaw];
     }];
}
-(void) stopMotionManager{
    [motionManager stopDeviceMotionUpdates];
   
}

-(void)gyroUpdate: (float)x : (float)y : (float)z
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized(self)
        {
            [self gyroUpdateMainThread:x:y:z];
        }
    });
}

-(void) saveLocation{
    [self stopMotionManager];
    [GroundScan3dSettingsViewController setValue:@"m_locationPosX" float:m_location.x];
    [GroundScan3dSettingsViewController setValue:@"m_locationPosY" float:m_location.y];
}

float g_LastX,g_LastY;
-(void)gyroUpdateMainThread: (float)x : (float)y : (float)z
{
    int XAngle = (x - g_LastX) * 500;
    int YAngle = (y - g_LastY) * 500;
   // NSLog(@"Gyro %d %d",XAngle,YAngle);
    
    g_LastX = x;
    g_LastY = y;
    
    if (XAngle > 60)
        XAngle = 60;
    if (YAngle > 60)
        YAngle = 60;
    
    if (XAngle < -60)
        XAngle = -60;
    if (YAngle < -60)
        YAngle = -60;
    
    CGPoint rotatePoint;
    rotatePoint.x = XAngle;
    rotatePoint.y = YAngle;
    
    NSValue *value = [NSValue valueWithBytes:&rotatePoint objCType:@encode(CGPoint)];
    @synchronized (m_lastpositions){
        [m_lastpositions addObject: value];
        if ([m_lastpositions count] > 40)
             [m_lastpositions removeObjectAtIndex:0];
    }
    
}

-(void) setScreenSize:(float) width :(float) height{
    m_screenSize.height = 1;
    m_screenSize.width = width / height;
    m_frameSizeinPix.height = height;
    m_frameSizeinPix.width = width;
    
   
}

-(SCNScene*) getScane{
    return m_scene;
}

-(SCNNode*)  getCamera{
    return m_cameranode;
}

- (void)renderer:(id <SCNSceneRenderer>)renderer updateAtTime:(NSTimeInterval)time{
    
    if (_aim_test != NULL){
        CGPoint newpoint;
        newpoint.x = (m_frameSizeinPix.width / 2.0 - m_location.x) * -1 + m_frameSizeinPix.width / 2.0;
        newpoint.y = (m_frameSizeinPix.height / 2.0 - m_location.y) * -1 + m_frameSizeinPix.height / 2.0;

        dispatch_async(dispatch_get_main_queue(), ^{

            {
                self->_aim_test.center = newpoint;
            }
        });
    }
    
    [self UpdateTransform];
}

-(CGPoint) CalcAVGPosition
{
    CGPoint m;
    
    float size = m_lastpositions.count;
    for( NSValue *value in m_lastpositions)
    {
        CGPoint structValue;
        [value getValue:&structValue];
        m.x += structValue.x;
        m.y += structValue.y;
        
    }
    
    m.x /= size;
    m.y /= size;
  
    if (m.x == NAN)
         m.x = 0;
    if (m.y == NAN)
         m.y = 0;
    return m;
}

-(bool) moveCircle:(int) state :(CGPoint)_position{
    
    bool Result =false;
    
    switch (state) {
        case 1: // Begin drag
        {
            
            double R = (_position.x - m_location.x) * (_position.x - m_location.x) + (_position.y - m_location.y) * (_position.y - m_location.y);
            if (R < 1000)
            {
                m_animationMove = YES;
                m_save_color = [self getColor];
                [self ChangeColor:[UIColor purpleColor]];
                [self LockColor];
                Result = true;
            }
        }
            break;
        case 2: // Move
            
            if (m_animationMove)
            {
                
                
                if (_position.x < 30 || (m_frameSizeinPix.width -_position.x) < 30)
                    return Result;

                if (_position.y < 100 || (m_frameSizeinPix.height -_position.y) < 200)
                    return Result;
                
                
                    
                m_location.x = _position.x;
                m_location.y = _position.y;
                
                Result = true;
            }
            
            break;
        case 3: // End drag
            if (m_animationMove)
            {
                m_animationMove = NO;
                [self UnlockColor];
                [self ChangeColor:m_save_color];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    if (self.delegate) {
                    //                        [self.delegate resetARstate];
                    //                    }
                });
                Result = true;
            }
            break;
        case 4: // Init value
            m_location.x = _position.x;
            m_location.y = _position.y;
            Result = true;
            break;
        default:
            break;
    }
    
    return Result;
}

-(void) UpdateTransform
{
    SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Group" recursively:YES];
   
    float CorrectX = (m_frameSizeinPix.width / 2.0- m_location.x)/(m_frameSizeinPix.width / 2.0);
    float CorrectY = (m_frameSizeinPix.height  / 2.0- m_location.y)/(m_frameSizeinPix.height / 2.0);
    SCNVector3 vecpos = SCNVector3Make(- CorrectX * m_screenSize.width , CorrectY * m_screenSize.height, 0);
    node.position = vecpos;
    @synchronized (m_lastpositions) {
        CGPoint angAngles = [self CalcAVGPosition];
        SCNVector3 angles =  SCNVector3Make(M_PI * angAngles.y / 180.0 * 3.0, M_PI * angAngles.x / 180.0 * 3.0, 0);
        if (isnan(angles.x))
            angles.x = 0;
        if (isnan(angles.y))
            angles.y = 0;
        if (isnan(angles.z))
            angles.z = 0;
        
//        node.eulerAngles = angles;
       // NSLog(@"Ang %f %f %f",angles.x,angles.y,angles.z);
    }

}


-(void) SetNormalState
{
   

    SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Torus" recursively:YES];
    
    [SCNTransaction begin];
    [SCNTransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [SCNTransaction setAnimationDuration:1];
    node.geometry.firstMaterial.multiply.contents = [UIImage imageNamed:@"CircleNormal"];

    [SCNTransaction commit];
    
    [node removeActionForKey:@"rotate"];
    
}

-(void) SetWaitState
{
    SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Torus" recursively:YES];
            
    [SCNTransaction begin];
    [SCNTransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [SCNTransaction setAnimationDuration:1];
    
    node.geometry.firstMaterial.multiply.contents = [UIImage imageNamed:@"CircleWait"];
    
    SCNAction *action = [SCNAction rotateByX:0 y:0 z:-2 duration:1];
    [SCNTransaction commit];
    
    [node runAction:[SCNAction repeatActionForever:action] forKey:@"rotate"];
    
    
}

-(void) AddPulse
{
    SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Torus" recursively:YES];
    
    if (![node actionForKey:@"pulse"])
    {
        
        SCNAction* pulseOutAction = [SCNAction scaleBy:10.0f/9.0f duration:0.4];
        SCNAction* pulseInAction = [SCNAction scaleBy:9.0f/10.0f duration:0.4];
        pulseOutAction.timingMode = SCNActionTimingModeEaseOut;
        pulseInAction.timingMode = SCNActionTimingModeEaseOut;
        

        [node runAction:[SCNAction repeatActionForever:[SCNAction sequence:@[pulseOutAction,pulseInAction]]] forKey:@"pulse"];

    }
    else
    {
        SCNAction *act =  [node actionForKey:@"pulse"];
        [act setSpeed:0.8];
    }
    
}

-(void) RemovePulse
{
    SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Torus" recursively:YES];
    
    SCNAction *act =  [node actionForKey:@"pulse"];
    [act setSpeed:0];
    
}

-(void) ChangeColor:(UIColor*) _color
{
    
    if (m_lockcolor)
        return;

    m_current_color = _color;
   SCNNode *node = [m_scene.rootNode  childNodeWithName:@"Torus" recursively:YES];
    
    [SCNTransaction begin];
    [SCNTransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [SCNTransaction setAnimationDuration:0.8];
    node.geometry.firstMaterial.diffuse.contents  = _color;
    [SCNTransaction commit];
  
    
}

-(UIColor*) getColor{
    return m_current_color;
}

-(void) LockColor{
    m_lockcolor = YES;
}

-(void) UnlockColor{
    m_lockcolor = NO;
}

@end
