//
//  UIARkitExtends.h
//  ARKitView
//
//  Created by N.D. on 16/07/2018.
//  Copyright © 2018 N.D. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

#import "UIARViewDefs.h"

API_AVAILABLE(ios(11.0))
@interface MathHelpers:NSObject

typedef struct  {
    SCNVector3 position;
    CGFloat distanceToRayOrigin;
    SCNVector3 featureHit;
    CGFloat featureDistanceToHitResult;
} FeatureHitTestResult;

typedef struct  {
    SCNVector3 origin;
    SCNVector3 direction;
} HitTestRay;

+(SCNVector3) positionFromTransform:(matrix_float4x4) _transform;

+(NSString*) NSStringFromSTransform3D:(SCNMatrix4)_transform;
+(NSString*) NSStringFromMTransform3D:(matrix_float4x4)_transform;


+(ARWorldCoordCalc) worldPositionFromScreenPosition:(ARSCNView*)sceneView :(CGPoint)position :(SCNVector3)objectPos :(BOOL) useInfinityPlane :(ARAnchor**) _anchor :(SCNVector3*) _planeHitPosition ;
+(BOOL) hitTestRayFromScreenPos:(ARSCNView*)sceneView :(CGPoint)point :(HitTestRay*)_hittestray;
+(BOOL) rayIntersectionWithHorizontalPlan:(SCNVector3)rayOrigin :(SCNVector3)direction :(CGFloat) planeY :(SCNVector3*) vector;
+(BOOL) HitTestByHorizontalPlaneFromCamera:(ARSCNView*)sceneView :(CGPoint)_ScreenLocation :(SCNVector3)objectPos :(SCNVector3*) _planeHitPosition ;
+(SCNVector3) Normalize:(SCNVector3) _vector;
+(CGFloat) Lenght:(SCNVector3) _vector;
// VectorMultiplication
+(SCNVector3) Cross:(SCNVector3)_vec1 :(SCNVector3)_vec2;
// ScalarMultiplication
+(CGFloat) Dot:(SCNVector3)_vec1 :(SCNVector3)_vec2;
+(SCNVector3) Mul:(SCNVector3)_vec1 :(SCNVector3)_vec2;
+(SCNVector3) MulF:(SCNVector3)_vec1 :(CGFloat)_f;
+(SCNVector3) Sum:(SCNVector3)_vec1 :(SCNVector3)_vec2;
+(SCNVector3) Sub:(SCNVector3)_vec1 :(SCNVector3)_vec2;
+(SCNVector3) MulVecMat:(SCNVector3)_vec :(SCNMatrix4) _mat;

@end


