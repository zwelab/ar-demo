
#import <UIKit/UIKit.h>
#import "UIARMainViewControllerDelegate.h"
#import "UIVerticalSlider.h"
#import "UIBatteryView.h"
#import <SceneKit/SceneKit.h>
#import "Objects/CircleNode2.h"
#import "../Extensions/UIRadarView/UIRadarView.h"

API_AVAILABLE(ios(11.0))
@interface ARInterfaceView : UIView  <ARCircleLogicDelegate,RadarViewDelegate>
{
  

    __weak IBOutlet UILabel *searchPlaneViewLabel;
    __weak IBOutlet UIView  *searchPlaneView;
                CircleNode2 *m_circle;
}

@property (strong, nonatomic) IBOutlet SCNView *curcleView;
@property (weak, nonatomic) IBOutlet UIImageView *aimimage;

@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (nonatomic, weak) id<ARMainViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIVerticalSlider *opacitySlider;
@property (weak, nonatomic) IBOutlet UIBatteryView *batteryView;
@property (weak, nonatomic) IBOutlet UIRadarView *m_radar;
    
-(id) init;
-(void) setSearchViewState:(BOOL)_visible :(NSString*)_text;

-(void) stopMotionManager;
-(void) startMotionManager;
-(void) startCalibration;
-(bool) moveCircle:(int) state :(CGPoint)_position;
-(void) willDisappear;
@end


