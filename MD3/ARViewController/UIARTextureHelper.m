

#import "UIARTextureHelper.h"


@implementation UIARTextureHelper


+(void) ConfiguratePlane:(ARPlaneGrid*)_grid :(ARPlaneGridItem*)_plane :(CGFloat)_magvalue :(CGFloat) _alpha
{
    _plane.value = _magvalue;
    _plane->m_texture_val[1][1] = _magvalue;
    [UIARTextureHelper GenerateTexture:_grid :_plane : _alpha];

}

+(void) GenerateTexture:(ARPlaneGrid*)_grid :(ARPlaneGridItem*)_plane :(CGFloat) _alpha
{
// Grid
//     (0)(1)(2)
//     (0)(1)(2)
//     (0)(1)(2)
    
    SCNVector3 baseIndex = _plane.indexPlane;
    
    for(int i = -1 ; i <= 1 ; i++)
        for(int j = -1 ; j <= 1 ; j++)
        {
            [UIARTextureHelper CheckValues:_grid :[_grid GetPlaneAtIndex:SCNVector3Make(baseIndex.x+i, baseIndex.y, baseIndex.z+j)]];
        }
    
    for(int i = -1 ; i <= 1 ; i++)
        for(int j = -1 ; j <= 1 ; j++)
        {
            [UIARTextureHelper BuildImage:[_grid GetPlaneAtIndex:SCNVector3Make(baseIndex.x+i, baseIndex.y, baseIndex.z+j)] :_alpha];
        }
}

+(void) BuildImage:(ARPlaneGridItem*)_plane :(CGFloat) _alpha
{
    if (!_plane)
        return;
    
    unsigned char *buff = (unsigned char *)malloc(36);
    
    int i = 0 , j = 0;
    
    for (int ptr = 0 ;ptr < 36; ptr += 4) {
        UIColor *col = [UIARTextureHelper valToColorWithAlfa:_plane->m_texture_val[j][i]];
        CGFloat r,g,b,a;
        [col getRed:&r green:&g blue:&b alpha:&a];

        buff[ptr ] = r  * 255;
        buff[ptr + 1] = g  * 255;
        buff[ptr + 2] = b  * 255;
        buff[ptr + 3] = a *  255;
        
        i++;
        if (i >= 3)
        {
            j++;
            i = 0;
        }
    }
    
    
    UIImage *processedImage = [UIARTextureHelper convertBitmapRGBA8ToUIImage:buff withWidth:3 withHeight:3];
    
    free(buff);
 
    _plane.geometry.firstMaterial.diffuse.contents = processedImage;
    _plane.geometry.firstMaterial.transparencyMode =SCNTransparencyModeDefault;
    _plane.geometry.firstMaterial.transparency = _alpha;
    
}

+ (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage {
    UIImage *image = nil;
    
    CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    
    UIGraphicsBeginImageContext(newImageSize);
    CGContextSetInterpolationQuality(UIGraphicsGetCurrentContext(), kCGInterpolationHigh);
    [firstImage drawInRect:CGRectMake(0,
                                       0,newImageSize.width,newImageSize.height)];
    [secondImage drawInRect:CGRectMake(0,
                                         0,newImageSize.width,newImageSize.height)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(void) CheckValues:(ARPlaneGrid*)_grid :(ARPlaneGridItem*)_plane
{
    if (!_plane)
        return;
    
    // Grid
    //     (0)(1)(2)
    //     (0)(1)(2)
    //     (0)(1)(2)
    float grid_items[3][3];
    SCNVector3 baseIndex = _plane.indexPlane;
    
    grid_items[0][0] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x-1, baseIndex.y, baseIndex.z-1)];
    grid_items[0][1] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x,   baseIndex.y, baseIndex.z-1)];
    grid_items[0][2] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x+1, baseIndex.y, baseIndex.z-1)];
    
    grid_items[1][0] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x-1, baseIndex.y, baseIndex.z)];
    grid_items[1][1] = _plane.value;
    grid_items[1][2] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x+1, baseIndex.y, baseIndex.z)];
    
    grid_items[2][0] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x-1, baseIndex.y, baseIndex.z+1)];
    grid_items[2][1] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x,   baseIndex.y, baseIndex.z+1)];
    grid_items[2][2] = [UIARTextureHelper GetPlaneIndexValue:_grid : SCNVector3Make(baseIndex.x+1, baseIndex.y, baseIndex.z+1)];
    
    _plane->m_texture_val[0][0] = [UIARTextureHelper SumOfFour:grid_items[1][1]:grid_items[0][1]:grid_items[1][0]:grid_items[0][0]];
    _plane->m_texture_val[0][1] = [UIARTextureHelper SumOfTwo: grid_items[1][1]:grid_items[0][1]];
    _plane->m_texture_val[0][2] = [UIARTextureHelper SumOfFour:grid_items[1][1]:grid_items[0][1]:grid_items[1][2]:grid_items[0][2]];
    
    _plane->m_texture_val[1][0] = [UIARTextureHelper SumOfTwo: grid_items[1][1]:grid_items[1][0]];
    _plane->m_texture_val[1][1] = grid_items[1][1] + 10;
    _plane->m_texture_val[1][2] = [UIARTextureHelper SumOfTwo: grid_items[1][1]:grid_items[1][2]];
    
    _plane->m_texture_val[2][0] = [UIARTextureHelper SumOfFour:grid_items[1][1]:grid_items[1][0]:grid_items[2][1]:grid_items[2][0]];
    _plane->m_texture_val[2][1] = [UIARTextureHelper SumOfTwo: grid_items[1][1]:grid_items[2][1]];
    _plane->m_texture_val[2][2] = [UIARTextureHelper SumOfFour:grid_items[1][1]:grid_items[2][1]:grid_items[1][2]:grid_items[2][2]];
    
    
}


// Alfa = 1000 = 0 ; 100 = 1

+(float) SumOfFour:(float)_coreX :(float)_coreDX :(float)_coreDZ :(float)_coreDXZ
{
    if ((_coreDX == 1010) && (_coreDZ == 1010)  && (_coreDXZ == 1010))
        return (_coreX + 1010);
    
    if ((_coreDX == 1010) && (_coreDZ == 1010)  && (_coreDXZ != 1010))
        return (_coreX + 1010);
    
    if ((_coreDX != 1010) && (_coreDZ != 1010)  && (_coreDXZ == 1010))
        return (_coreX +_coreDX + _coreDZ) / 3 + 1010;
    
    if ((_coreDX != 1010) && (_coreDZ == 1010)  && (_coreDXZ == 1010))
        return (_coreX +_coreDX) / 2 + 1010;
    
    if ((_coreDX == 1010) && (_coreDZ != 1010)  && (_coreDXZ == 1010))
        return (_coreX +_coreDZ) / 2 + 1010;
    
    if ((_coreDX == 1010) && (_coreDZ != 1010)  && (_coreDXZ != 1010))
        return (_coreX +_coreDZ + _coreDXZ) / 3 + 1010;
    
    if ((_coreDX != 1010) && (_coreDZ == 1010)  && (_coreDXZ != 1010))
        return (_coreX +_coreDX + _coreDXZ) / 3 + 1010;
    
    if ((_coreDX != 1010) && (_coreDZ != 1010)  && (_coreDXZ != 1010))
        return (_coreX +_coreDX + _coreDXZ + _coreDZ) / 4 + 10;
    
    return -666;
}

+(float) SumOfTwo:(float)_core :(float)_coreD
{
    if (_core == 1010 && _coreD == 1010)
        return 1010;
    if (_coreD == 1010)
        return (_core + 1010);
    if (_core == 1010)
        return (_coreD + 1010);
    return ((_core + _coreD)/2 + 10);
}

+(float) GetPlaneIndexValue:(ARPlaneGrid*)_grid :(SCNVector3)_index
{
    ARPlaneGridItem *node  =  [_grid GetPlaneAtIndex:_index];
    if (node)
    {
        return node.value;
    }
    else
        return 1010;
}

+(UIColor*) valToColor:(CGFloat)_val
{
    float r = 0;
    float b = 1;
    float g = 0;
    
    float val = _val;
    if((val < 0.5) && (val >= 0)){
        g = 1;
        r = val * 2;
        b = 0;
    }
    else if((val >= 0.5) && (val <= 1.0)){
        g = -2.0 * val + 2.0;
        r = 1;
        b = 0;
    }
    else if((val < 0) && (val >= -0.5)){
        g = 1;
        r = 0;
        b = val * -2.0;
    }
    else if((val < -0.5) && (val >= -1.0)){
        g = 2.0 * val + 2.0;
        r = 0;
        b = 1;
    }

    return [UIColor colorWithRed:r
                           green:g
                            blue:b
                           alpha:1.0f];
}

+(UIColor*) valToColorWithAlfa:(CGFloat)_val
{
    float r = 0;
    float b = 1;
    float g = 0;
    
    float A = (10 - ((int)_val / 100));
    
    _val = (_val - ((int)_val / 100) * 100) - 10;
    
    float val = _val;
    if((val < 0.5) && (val >= 0)){
        g = 1;
        r = val * 2;
        b = 0;
    }
    else if((val >= 0.5) && (val <= 1.0)){
        g = -2.0 * val + 2.0;
        r = 1;
        b = 0;
    }
    else if((val < 0) && (val >= -0.5)){
        g = 1;
        r = 0;
        b = val * -2.0;
    }
    else if((val < -0.5) && (val >= -1.0)){
        g = 2.0 * val + 2.0;
        r = 0;
        b = 1;
    }
    
    return [UIColor colorWithRed:r
                           green:g
                            blue:b
                           alpha:A / 10];
}


+ (UIImage *) convertBitmapRGBA8ToUIImage:(unsigned char *) buffer
                                withWidth:(int) width
                               withHeight:(int) height {
    
    
    size_t bufferLength = width * height * 4;
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, bufferLength, NULL);
    size_t bitsPerComponent = 8;
    size_t bitsPerPixel = 32;
    size_t bytesPerRow = 4 * width;
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    if(colorSpaceRef == NULL) {
        NSLog(@"Error allocating color space");
        CGDataProviderRelease(provider);
        return nil;
    }
    
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrder32Big | kCGImageAlphaLast;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    CGImageRef iref = CGImageCreate(width,
                                    height,
                                    bitsPerComponent,
                                    bitsPerPixel,
                                    bytesPerRow,
                                    colorSpaceRef,
                                    bitmapInfo,
                                    provider,    // data provider
                                    NULL,        // decode
                                    NO,            // should interpolate
                                    renderingIntent);
    
    
    
    uint32_t* pixels = (uint32_t*)malloc(bufferLength);
    for(int i = 0 ; i < bufferLength/4;i ++)
        pixels[i] = 0;
    
    if(pixels == NULL) {
        NSLog(@"Error: Memory not allocated for bitmap");
        CGDataProviderRelease(provider);
        CGColorSpaceRelease(colorSpaceRef);
        CGImageRelease(iref);
        return nil;
    }
    
    bitmapInfo =  kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedFirst;

    CGContextRef context = CGBitmapContextCreate(pixels,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpaceRef,
                                                 bitmapInfo);
    
    if(context == NULL) {
        NSLog(@"Error context not created");
        free(pixels);
    }
    
    UIImage *image = nil;
    if(context) {
        CGContextSetBlendMode(context, kCGBlendModeCopy);
        CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), iref);
        
        CGImageRef imageRef = CGBitmapContextCreateImage(context);
        
        image = [UIImage imageWithCGImage:imageRef];
        
        CGImageRelease(imageRef);
        CGContextRelease(context);
    }
    
    CGColorSpaceRelease(colorSpaceRef);
    CGImageRelease(iref);
    CGDataProviderRelease(provider);
    
    if(pixels) {
        free(pixels);
    }
    return image;
}


@end
