
#import "UIARkitExtends.h"

@implementation MathHelpers


enum TAGS {
    FILE_SAVE_TAG = 1,
    REWRITE_TAG,
    BUY_TAG,
    BALANCED_TAG,
    SHARE_TAG,
    DEPTH_TAG
};


+(SCNVector3) positionFromTransform:(matrix_float4x4) _transform
{
    return SCNVector3Make(_transform.columns[3].x, _transform.columns[3].y, _transform.columns[3].z);
}

+(NSString*) NSStringFromSTransform3D:(SCNMatrix4)_transform
{
    return [NSString stringWithFormat:@"\n[%f, %f, %f, %f\n %f, %f, %f, %f\n %f, %f, %f, %f\n %f, %f, %f, %f]",
            _transform.m11, _transform.m12, _transform.m13, _transform.m14,
            _transform.m21, _transform.m22, _transform.m23, _transform.m24,
            _transform.m31, _transform.m32, _transform.m33, _transform.m34,
            _transform.m41, _transform.m42, _transform.m43, _transform.m44];
}


+(NSString*) NSStringFromMTransform3D:(matrix_float4x4)_transform
{
    return [NSString stringWithFormat:@"\n[%f, %f, %f, %f\n %f, %f, %f, %f\n %f, %f, %f, %f\n %f, %f, %f, %f]",
            _transform.columns[0].x, _transform.columns[0].y, _transform.columns[0].z, _transform.columns[0].w,
            _transform.columns[1].x, _transform.columns[1].y, _transform.columns[1].z, _transform.columns[1].w,
            _transform.columns[2].x, _transform.columns[2].y, _transform.columns[2].z, _transform.columns[2].w,
            _transform.columns[3].x, _transform.columns[3].y, _transform.columns[3].z, _transform.columns[3].w];
            
}

+(ARWorldCoordCalc) worldPositionFromScreenPosition:(ARSCNView*)sceneView :(CGPoint)position :(SCNVector3)objectPos :(BOOL) useInfinityPlane :(ARAnchor**) _anchor :(SCNVector3*) _planeHitPosition
{
    // -------------------------------------------------------------------------------
    // 1. Always do a hit test against exisiting plane anchors first.
    //    (If any such anchors exist & only within their extents.)
    
    NSArray<ARHitTestResult*> *result = [sceneView hitTest:position types:ARHitTestResultTypeExistingPlaneUsingExtent];
    ARHitTestResult* resultHit = [result firstObject];
    if (resultHit)
    {
        
        SCNVector3 planeHitTestPosition =[MathHelpers positionFromTransform:resultHit.worldTransform];
        *_anchor = resultHit.anchor;
        *_planeHitPosition = planeHitTestPosition;
        return ARWorldCoordCalcPlane;
        //    return (planeHitTestPosition, planeAnchor as? ARPlaneAnchor, true)
    }
    
    // -------------------------------------------------------------------------------
    // 2. Collect more information about the environment by hit testing against
    //    the feature point cloud, but do not return the result yet.
    
    SCNVector3 featureHitTestPosition = SCNVector3Make(0,0,0);
    BOOL highQualityFeatureHitTestResult = YES;
    
    NSArray* highQualityfeatureHitTestResults = [MathHelpers  hitTestWithFeatures:sceneView :position :18 :0.2 :2];
    
    if (highQualityfeatureHitTestResults.count > 0) {
        FeatureHitTestResult result;
        [[highQualityfeatureHitTestResults firstObject] getValue:&result];
        featureHitTestPosition = result.position;
        highQualityFeatureHitTestResult = true;
    }
    
    if (useInfinityPlane || !highQualityFeatureHitTestResult) {
        
        SCNVector3 pointOnInfinitePlane = SCNVector3Make(0, 0, 0);
        if ([MathHelpers hitTestWithInfiniteHorizontalPlane:sceneView :position :objectPos :&pointOnInfinitePlane])
        {
            *_planeHitPosition = pointOnInfinitePlane;
            return ARWorldCoordCalcInfintyPlane;
        }
    }
    
    if (highQualityFeatureHitTestResult) {
        *_planeHitPosition = featureHitTestPosition;
        return ARWorldCoordCalcFeaturePoints;
    }
    
    return ARWorldCoordCalcNone;
}

+(BOOL) HitTestByHorizontalPlaneFromCamera:(ARSCNView*)sceneView :(CGPoint)_ScreenLocation :(SCNVector3)objectPos :(SCNVector3*) _planeHitPosition
API_AVAILABLE(ios(11.0)){
    SCNVector3 pointOnInfinitePlane = SCNVector3Make(0, 0, 0);
    if ([MathHelpers hitTestWithInfiniteHorizontalPlane:sceneView :_ScreenLocation :objectPos :&pointOnInfinitePlane])
    {
        *_planeHitPosition = pointOnInfinitePlane;
        return YES;
    }
    return NO;
}


+(NSArray*) hitTestWithFeatures:(ARSCNView*)sceneView :(CGPoint)point :(CGFloat)coneOpeningAngleInDegrees :(CGFloat)minDistance :(CGFloat)maxDistance {
    return [MathHelpers hitTestWithFeatures:sceneView:point:coneOpeningAngleInDegrees:minDistance:maxDistance:1];
}

+(NSArray*) hitTestWithFeatures:(ARSCNView*)sceneView :(CGPoint)point :(CGFloat)coneOpeningAngleInDegrees :(CGFloat)minDistance {
     return [MathHelpers hitTestWithFeatures:sceneView:point:coneOpeningAngleInDegrees:minDistance:CGFLOAT_MAX:1];
}

+(NSArray*) hitTestWithFeatures:(ARSCNView*)sceneView :(CGPoint)point :(CGFloat)coneOpeningAngleInDegrees {
    return [MathHelpers hitTestWithFeatures:sceneView:point:coneOpeningAngleInDegrees:0:CGFLOAT_MAX:1];
}

+(NSArray*) hitTestWithFeatures:(ARSCNView*)sceneView :(CGPoint)point :(CGFloat)coneOpeningAngleInDegrees :(CGFloat)minDistance :(CGFloat)maxDistance :(int)maxResults  {
     
    NSMutableArray *results = [[NSMutableArray alloc] init];
    if (sceneView.session.currentFrame)
    {
        ARPointCloud* features = sceneView.session.currentFrame.rawFeaturePoints;
        if (!features)
            return results;
        
        HitTestRay ray;
        if (![MathHelpers hitTestRayFromScreenPos:sceneView:point:&ray]){
            return results;
        }
        
        CGFloat maxAngleInDeg = MIN(coneOpeningAngleInDegrees, 360) / 2.0;
        CGFloat maxAngle = ((maxAngleInDeg / 180) * M_PI);
        
        const vector_float3 *points = features.points;
        for(int i = 0 ; i < features.count ; i++)
        {
            vector_float3 point =  points[i];
            SCNVector3 pointVec = SCNVector3Make(point[0], point[1],point[2]);
            SCNVector3 originToFeature = [MathHelpers Sub:pointVec:ray.origin];
            SCNVector3 crossProduct = [MathHelpers Cross:originToFeature :ray.direction];
            CGFloat featureDistanceFromResult = [MathHelpers Lenght:crossProduct];
            
            SCNVector3 hitTestResult = [MathHelpers Sum: ray.origin : [MathHelpers MulF:ray.direction : [MathHelpers  Dot:ray.direction:originToFeature]]];
            CGFloat hitTestResultDistance = [MathHelpers Lenght:[MathHelpers Sub:hitTestResult: ray.origin]];
            
            // Skip points not in range
            if (hitTestResultDistance < minDistance || hitTestResultDistance > maxDistance) {
                continue;
            }
            
            SCNVector3 originToFeatureNormalized  = [MathHelpers Normalize:originToFeature];
            CGFloat angleBetweenRayAndFeature = acosf([MathHelpers Dot:ray.direction :originToFeatureNormalized]);
            
            // Skip points not in angle  of cone
            if (angleBetweenRayAndFeature > maxAngle) {
                continue;
            }
            FeatureHitTestResult fhts = (FeatureHitTestResult){.position = hitTestResult,.distanceToRayOrigin = hitTestResultDistance,.featureHit = pointVec,.featureDistanceToHitResult = featureDistanceFromResult};
            NSValue *value = [NSValue valueWithBytes:&fhts objCType:@encode(FeatureHitTestResult)];
            [results addObject:value];
            
        }
        [results sortUsingComparator:^NSComparisonResult (id a, id b){
            
            NSValue* itemA = (NSValue*)a;
            NSValue* itemB = (NSValue*)b;
            FeatureHitTestResult structValue1;
            FeatureHitTestResult structValue2;
            [itemA getValue:&structValue1];
            [itemB getValue:&structValue2];
            
            return structValue1.distanceToRayOrigin < structValue2.distanceToRayOrigin; }];
    }
    NSUInteger count = MIN( results.count , maxResults );
    return [results subarrayWithRange: NSMakeRange( 0, count )];
 }

+(BOOL) hitTestRayFromScreenPos:(ARSCNView*)sceneView :(CGPoint)point :(HitTestRay*)_hittestray  API_AVAILABLE(ios(11.0)){
    
    ARFrame *frame = sceneView.session.currentFrame;
    if (!frame)
        return NO;
    
    SCNVector3 cameraPos = [MathHelpers positionFromTransform:frame.camera.transform];
    SCNVector3 positionVec = SCNVector3Make((CGFloat)point.x, (CGFloat)point.y, 1.0);
    SCNVector3 screenPosOnFarClippingPlane = [sceneView unprojectPoint:positionVec];
    
    SCNVector3 rayDirection = SCNVector3Make(screenPosOnFarClippingPlane.x - cameraPos.x,screenPosOnFarClippingPlane.y - cameraPos.y,screenPosOnFarClippingPlane.z - cameraPos.z);

    rayDirection = [MathHelpers Normalize:rayDirection];
    *_hittestray = (HitTestRay){.origin = cameraPos, .direction = rayDirection};
    return YES;
}
                                
+(BOOL) hitTestWithInfiniteHorizontalPlane:(ARSCNView*)sceneView :(CGPoint)_point :(SCNVector3)_pointOnPlane :(SCNVector3*)_vector  API_AVAILABLE(ios(11.0)){
    
    HitTestRay ray;
    if (![MathHelpers hitTestRayFromScreenPos:sceneView:_point :&ray])
    {
        return NO;
    }
    
    // Do not intersect with planes above the camera or if the ray is almost parallel to the plane.
    if (ray.direction.y > -0.03) {
        return NO;
    }
    
    // Return the intersection of a ray from the camera through the screen position with a horizontal plane
    // at height (Y axis).
    
    return [MathHelpers rayIntersectionWithHorizontalPlan: ray.origin: ray.direction:_pointOnPlane.y:_vector];
    
}

+(BOOL) rayIntersectionWithHorizontalPlan:(SCNVector3)rayOrigin :(SCNVector3)direction :(CGFloat) planeY :(SCNVector3*) vector{
    
    direction = [MathHelpers Normalize:direction];
    
    // Special case handling: Check if the ray is horizontal as well.
    if (direction.y == 0) {
        if (rayOrigin.y == planeY) {
            // The ray is horizontal and on the plane, thus all points on the ray intersect with the plane.
            // Therefore we simply return the ray origin.
            *vector = rayOrigin;
            return YES;
        } else {
            // The ray is parallel to the plane and never intersects.
            return NO;
        }
    }
    
    // The distance from the ray's origin to the intersection point on the plane is:
    //   (pointOnPlane - rayOrigin) dot planeNormal
    //  --------------------------------------------
    //          direction dot planeNormal
    
    // Since we know that horizontal planes have normal (0, 1, 0), we can simplify this to:
    CGFloat dist = (planeY - rayOrigin.y) / direction.y;
    
    // Do not return intersections behind the ray's origin.
    if (dist < 0) {
        return NO;
    }
    
    // Return the intersection point.
    *vector =  [MathHelpers Sum: rayOrigin : [MathHelpers MulF:direction : dist]];
    return YES;
}
                                    
+(SCNVector3) Normalize:(SCNVector3) _vector
{
    CGFloat len = sqrtf(_vector.x * _vector.x + _vector.y  * _vector.y + _vector.z * _vector.z);
    return SCNVector3Make(_vector.x / len, _vector.y / len, _vector.z / len);
}

+(CGFloat) Lenght:(SCNVector3) _vector
{
    return sqrtf(_vector.x * _vector.x + _vector.y  * _vector.y + _vector.z * _vector.z);
}


+(SCNVector3) Cross:(SCNVector3)_vec1 :(SCNVector3)_vec2
{
    return SCNVector3Make(_vec1.y * _vec2.z - _vec1.z * _vec2.y, _vec1.z * _vec2.x - _vec1.x * _vec2.z, _vec1.x * _vec2.y - _vec1.y * _vec2.x);
}

+(CGFloat) Dot:(SCNVector3)_vec1 :(SCNVector3)_vec2
{
    return (_vec1.x * _vec2.x) + (_vec1.y * _vec2.y) + (_vec1.z * _vec2.z);
}

+(SCNVector3) Mul:(SCNVector3)_vec1 :(SCNVector3)_vec2
{
    return SCNVector3Make(_vec1.x * _vec2.x , _vec1.y * _vec2.y , _vec1.z * _vec2.z );
}

+(SCNVector3) Sum:(SCNVector3)_vec1 :(SCNVector3)_vec2
{
    return SCNVector3Make(_vec1.x + _vec2.x , _vec1.y + _vec2.y , _vec1.z + _vec2.z );
}

+(SCNVector3) Sub:(SCNVector3)_vec1 :(SCNVector3)_vec2
{
   return SCNVector3Make(_vec1.x - _vec2.x , _vec1.y - _vec2.y , _vec1.z - _vec2.z );
}

+(SCNVector3) MulF:(SCNVector3)_vec1 :(CGFloat)_f
{
    return SCNVector3Make(_vec1.x * _f , _vec1.y * _f , _vec1.z * _f );
}

+(SCNVector3) MulVecMat:(SCNVector3)_vec :(SCNMatrix4) _mat
{
    SCNVector3 vec = SCNVector3Make(0, 0, 0);
    
    vec.x = _vec.x * _mat.m11 + _vec.y * _mat.m21 + _vec.z * _mat.m31 +  _mat.m41;
    vec.y = _vec.x * _mat.m12 + _vec.y * _mat.m22 + _vec.z * _mat.m32 +  _mat.m42;
    vec.z = _vec.x * _mat.m13 + _vec.y * _mat.m23 + _vec.z * _mat.m33 +  _mat.m43;
    
    return vec;
}



@end
