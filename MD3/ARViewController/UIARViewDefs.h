

#ifndef UIARViewDefs_h
#define UIARViewDefs_h

typedef enum{
    ARErrorWorldTrackingIsNotSupport=0
} ARViewErrorCodes;

typedef enum{
    ARWorldCoordCalcNone = 0,
    ARWorldCoordCalcPlane,
    ARWorldCoordCalcInfintyPlane,
    ARWorldCoordCalcFeaturePoints
} ARWorldCoordCalc;

#endif /* UIARViewDefs_h */
